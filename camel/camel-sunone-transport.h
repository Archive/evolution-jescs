/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/* camel-sunone-transport.h: SunOne-based transport class */


#ifndef CAMEL_SUNONE_TRANSPORT_H
#define CAMEL_SUNONE_TRANSPORT_H 1


#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus }*/

#include <camel/camel-transport.h>

#define CAMEL_SUNONE_TRANSPORT_TYPE     (camel_sunone_transport_get_type ())
#define CAMEL_SUNONE_TRANSPORT(obj)     (CAMEL_CHECK_CAST((obj), CAMEL_SUNONE_TRANSPORT_TYPE, CamelSunOneTransport))
#define CAMEL_SUNONE_TRANSPORT_CLASS(k) (CAMEL_CHECK_CLASS_CAST ((k), CAMEL_SUNONE_TRANSPORT_TYPE, CamelSunOneTransportClass))
#define CAMEL_IS_SUNONE_TRANSPORT(o)    (CAMEL_CHECK_TYPE((o), CAMEL_SUNONE_TRANSPORT_TYPE))


typedef struct {
	CamelTransport parent_object;

} CamelSunOneTransport;


typedef struct {
	CamelTransportClass parent_class;

} CamelSunOneTransportClass;


/* Standard Camel function */
CamelType camel_sunone_transport_get_type (void);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* CAMEL_SUNONE_TRANSPORT_H */
