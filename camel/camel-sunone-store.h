/* 
 * Copyright (C) 2002-2005 Sun Microsystems, Inc
 *
 * AUTHORS
 *     Harry Lu <harry.lu@sun.com>
 *                              
 */

#ifndef CAMEL_SUNONE_STORE_H
#define CAMEL_SUNONE_STORE_H 1

#include <camel/camel-store.h>

G_BEGIN_DECLS

#define CAMEL_SUNONE_STORE_TYPE     (camel_sunone_store_get_type ())
#define CAMEL_SUNONE_STORE(obj)     (CAMEL_CHECK_CAST((obj), CAMEL_SUNONE_STORE_TYPE, CamelSunOneStore))
#define CAMEL_SUNONE_STORE_CLASS(k) (CAMEL_CHECK_CLASS_CAST ((k), CAMEL_SUNONE_STORE_TYPE, CamelSunOneStoreClass))
#define CAMEL_IS_SUNONE_STORE(o)    (CAMEL_CHECK_TYPE((o), CAMEL_SUNONE_STORE_TYPE))

typedef struct {
	CamelStore parent_object;	
} CamelSunOneStore;

typedef struct {
	CamelStoreClass parent_class;
} CamelSunOneStoreClass;

/* Standard Camel function */
CamelType camel_sunone_store_get_type (void);

G_END_DECLS

#endif /* CAMEL_SUNONE_STORE_H */


