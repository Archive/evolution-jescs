/* camel-sunone-provider.c
 *
 * Copyright (C) 2002-2004 Sun Microsystems, Inc
 *
 * AUTHORS
 *     Harry Lu <harry.lu@sun.com>
 *     Alfred Peng <alfred.peng@sun.com>
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <camel/camel-i18n.h>

#include "camel-sunone-transport.h"
#include "camel-sunone-store.h"

static guint wcap_url_hash (gconstpointer key);
static gint check_equal (char *s1, char *s2);
static gint wcap_url_equal (gconstpointer a, gconstpointer b);

CamelProviderConfEntry wcap_conf_entries[] = {
	/* override the labels/defaults of the standard settings */

	{ CAMEL_PROVIDER_CONF_LABEL, "username", NULL, N_("_User") },

	/* extra SunOne configuration settings */
	{ CAMEL_PROVIDER_CONF_SECTION_START, "calendar", NULL, N_("Calendar") },
	{ CAMEL_PROVIDER_CONF_ENTRY, "serverurl", NULL, N_("_Server URL:"), NULL },
	{ CAMEL_PROVIDER_CONF_CHECKBOX, "useproxy", NULL, N_("_Use System Proxy Settings"), "1" },
	{ CAMEL_PROVIDER_CONF_SECTION_END },
	/* ... */

	{ CAMEL_PROVIDER_CONF_END }
};


static CamelProvider wcap_provider = {
	"wcap",
	N_("Sun Calendar WCAP"),
	N_("For accessing Sun Calendar servers"),
	"mail",
	CAMEL_PROVIDER_IS_REMOTE | CAMEL_PROVIDER_IS_SOURCE,

	CAMEL_URL_NEED_USER,

	wcap_conf_entries,

	/* ... */
};

void
camel_provider_module_init(void)
{
	wcap_provider.url_hash = wcap_url_hash;
	wcap_provider.url_equal = wcap_url_equal;

#if 0
	bindtextdomain (GETTEXT_PACKAGE, EVOLUTION_LOCALEDIR);
	wcap_provider.translation_domain = GETTEXT_PACKAGE;
#endif

	wcap_provider.object_types[CAMEL_PROVIDER_STORE] =  camel_sunone_store_get_type ();
	wcap_provider.object_types[CAMEL_PROVIDER_TRANSPORT] =  camel_sunone_transport_get_type ();
	camel_provider_register(&wcap_provider);

}

static void
add_hash (guint *hash, char *s)
{
	if (s)
		*hash ^= g_str_hash(s);
}

static guint
wcap_url_hash (gconstpointer key)
{
	const CamelURL *u = (CamelURL *)key;
	guint hash = 0;

	add_hash (&hash, u->user);
	add_hash (&hash, u->host);
	
	return hash;
}

static gint
check_equal (char *s1, char *s2)
{
	if (!s1)
		return s2 == NULL;
	
	if (!s2)
		return FALSE;

	return strcmp (s1, s2) == 0;
}

static gint
wcap_url_equal (gconstpointer a, gconstpointer b)
{
	const CamelURL *u1 = a, *u2 = b;
	
	return check_equal (u1->user, u2->user)
		&& check_equal (u1->host, u2->host);
}
