/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/* camel-sunone-transport.c: SunOne transport class. */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "camel-sunone-transport.h"

#include <camel/camel-data-wrapper.h>
#include <camel/camel-exception.h>
#include <camel/camel-mime-filter-crlf.h>
#include <camel/camel-mime-message.h>
#include <camel/camel-session.h>
#include <camel/camel-stream-filter.h>
#include <camel/camel-stream-mem.h>
#include <camel/camel-i18n.h>

#include <string.h>

static gboolean sunone_send_to (CamelTransport *transport,
				  CamelMimeMessage *message,
				  CamelAddress *from,
				  CamelAddress *recipients,
				  CamelException *ex);


static void
camel_sunone_transport_class_init (CamelSunOneTransportClass *camel_sunone_transport_class)
{
	CamelTransportClass *camel_transport_class =
		CAMEL_TRANSPORT_CLASS (camel_sunone_transport_class);

	/* virtual method overload */
	camel_transport_class->send_to = sunone_send_to;
}

static void
camel_sunone_transport_init (CamelTransport *transport)
{
}

CamelType
camel_sunone_transport_get_type (void)
{
	static CamelType camel_sunone_transport_type = CAMEL_INVALID_TYPE;

	if (camel_sunone_transport_type == CAMEL_INVALID_TYPE) {
		camel_sunone_transport_type =
			camel_type_register (CAMEL_TRANSPORT_TYPE,
					     "CamelSunOneTransport",
					     sizeof (CamelSunOneTransport),
					     sizeof (CamelSunOneTransportClass),
					     (CamelObjectClassInitFunc) camel_sunone_transport_class_init,
					     NULL,
					     (CamelObjectInitFunc) camel_sunone_transport_init,
					     NULL);
	}

	return camel_sunone_transport_type;
}


static gboolean
sunone_send_to (CamelTransport *transport, CamelMimeMessage *message,
		  CamelAddress *from, CamelAddress *recipients,
		  CamelException *ex)
{
	camel_exception_set (ex, CAMEL_EXCEPTION_SYSTEM,
		_("Send operation is not supported by Sun WCAP protocol"));
	return FALSE;
}
