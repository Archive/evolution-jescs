/*
 *
 * Copyright (C) 2002-2005 Sun Microsystems, Inc
 *
 * AUTHORS
 *     Harry Lu <harry.lu@sun.com>
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <camel/camel-sunone-store.h>

#define d(x) 

/* Returns the class for a CamelSunOneStore */
#define CSUNONES_CLASS(so) CAMEL_SUNONE_STORE_CLASS (CAMEL_OBJECT_GET_CLASS(so))
#define CF_CLASS(so) CAMEL_FOLDER_CLASS (CAMEL_OBJECT_GET_CLASS(so))

static CamelFolder *get_folder(CamelStore * store, const char *folder_name, guint32 flags, CamelException * ex);

#if 0

static void construct (CamelService *service, CamelSession *session, CamelProvider *provider, CamelURL *url, CamelException *ex);
static CamelFolder *sunone_get_inbox (CamelStore *store, CamelException *ex);
static CamelFolder *sunone_get_junk(CamelStore *store, CamelException *ex);
static CamelFolder *sunone_get_trash(CamelStore *store, CamelException *ex);
static CamelFolderInfo *get_folder_info (CamelStore *store, const char *top, guint32 flags, CamelException *ex);
static void delete_folder(CamelStore *store, const char *folder_name, CamelException *ex);
static void rename_folder(CamelStore *store, const char *old, const char *new, CamelException *ex);
static CamelFolderInfo *create_folder(CamelStore *store, const char *parent_name, const char *folder_name, CamelException *ex);

#endif


static CamelStoreClass *parent_class = NULL;

static void
camel_sunone_store_class_init (CamelSunOneStoreClass *camel_sunone_store_class)
{
	CamelStoreClass *camel_store_class = CAMEL_STORE_CLASS (camel_sunone_store_class);
#if 0
	CamelServiceClass *camel_service_class = CAMEL_SERVICE_CLASS (camel_sunone_store_class);
#endif
	
	parent_class = CAMEL_STORE_CLASS (camel_type_get_global_classfuncs (camel_store_get_type ()));

	/* virtual method overload */
	camel_store_class->get_folder = get_folder;

#if 0
	camel_service_class->construct = construct;
	camel_service_class->get_name = get_name;
	camel_store_class->get_inbox = sunone_get_inbox;
	camel_store_class->get_trash = sunone_get_trash;
	camel_store_class->get_junk = sunone_get_junk;
	camel_store_class->get_folder_info = get_folder_info;
	camel_store_class->free_folder_info = camel_store_free_folder_info_full;

	camel_store_class->create_folder = create_folder;
	camel_store_class->delete_folder = delete_folder;
	camel_store_class->rename_folder = rename_folder;

	camel_sunone_store_class->get_full_path = sunone_get_full_path;
	camel_sunone_store_class->get_meta_path = sunone_get_meta_path;
#endif
}

static void
camel_sunone_store_finalize (CamelSunOneStore *sunone_store)
{
}

CamelType
camel_sunone_store_get_type (void)
{
	static CamelType camel_sunone_store_type = CAMEL_INVALID_TYPE;
	
	if (camel_sunone_store_type == CAMEL_INVALID_TYPE)	{
		camel_sunone_store_type = camel_type_register (CAMEL_STORE_TYPE, "CamelSunOneStore",
							     sizeof (CamelSunOneStore),
							     sizeof (CamelSunOneStoreClass),
							     (CamelObjectClassInitFunc) camel_sunone_store_class_init,
							     NULL,
							     NULL,
							     (CamelObjectFinalizeFunc) camel_sunone_store_finalize);
	}
	
	return camel_sunone_store_type;
}

static CamelFolder *
get_folder(CamelStore * store, const char *folder_name, guint32 flags, CamelException * ex)
{
	return NULL;
}

#if 0
static void
construct (CamelService *service, CamelSession *session, CamelProvider *provider, CamelURL *url, CamelException *ex)
{
	CAMEL_SERVICE_CLASS (parent_class)->construct (service, session, provider, url, ex);
}


static CamelFolder *
sunone_get_inbox(CamelStore *store, CamelException *ex)
{
	return NULL;
}

static CamelFolder *
sunone_get_trash(CamelStore *store, CamelException *ex)
{
	CamelFolder *folder = CAMEL_STORE_CLASS(parent_class)->get_trash(store, ex);
	return folder;
}

static CamelFolder *
sunone_get_junk(CamelStore *store, CamelException *ex)
{
	CamelFolder *folder = CAMEL_STORE_CLASS(parent_class)->get_junk(store, ex);
	return folder;
}

static CamelFolderInfo *
get_folder_info (CamelStore *store, const char *top,
		 guint32 flags, CamelException *ex)
{
	return NULL;
}

static CamelFolderInfo *
create_folder(CamelStore *store, const char *parent_name, const char *folder_name, CamelException *ex)
{
	CamelFolderInfo *info = NULL;

	return info;
}

static int xrename(const char *oldp, const char *newp, const char *prefix, const char *suffix, int missingok, CamelException *ex)
{
	int ret = -1;

	return ret;
}

static void
rename_folder(CamelStore *store, const char *old, const char *new, CamelException *ex)
{
}

static void
delete_folder(CamelStore *store, const char *folder_name, CamelException *ex)
{
}

static char *
sunone_get_full_path(CamelSunOneStore *ls, const char *full_name)
{
	return NULL;
}

static char *
sunone_get_meta_path(CamelSunOneStore *ls, const char *full_name, const char *ext)
{
	return NULL;
}
#endif

