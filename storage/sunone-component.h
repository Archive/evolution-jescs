/* sunone-component.h
 *
 * Copyright (C) 2002-2004 Sun Microsystems, Inc
 *
 * AUTHORS
 *     Jack Jia <jack.jia@sun.com>
 *     Harry Lu <harry.lu@sun.com>
 *     Alfred Peng <alfred.peng@sun.com>
 *     Jedy Wang <jedy.wang@sun.com>
 *
 */

#ifndef __SUNONE_COMPONENT_H__
#define __SUNONE_COMPONENT_H__

#include <bonobo/bonobo-object.h>
#include <shell/Evolution.h>
#include "sunone-account.h"
#include "sunone-offline-listener.h"

G_BEGIN_DECLS

#define SUNONE_TYPE_COMPONENT               (sunone_component_get_type ())
#define SUNONE_COMPONENT(obj)               (G_TYPE_CHECK_INSTANCE_CAST ((obj), SUNONE_TYPE_COMPONENT, SunOneComponent))
#define SUNONE_COMPONENT_CLASS(klass)       (G_TYPE_CHECK_CLASS_CAST ((klass), SUNONE_TYPE_COMPONENT, SunOneComponentClass))
#define SUNONE_IS_COMPONENT(obj)            (G_TYPE_CHECK_INSTANCE_TYPE ((obj), SUNONE_TYPE_COMPONENT))
#define SUNONE_IS_COMPONENT_CLASS(klass)    (G_TYPE_CHECK_CLASS_TYPE ((klass), SUNONE_TYPE_COMPONENT))

typedef struct _SunOneComponent                SunOneComponent;
typedef struct _SunOneComponentPrivate         SunOneComponentPrivate;
typedef struct _SunOneComponentClass           SunOneComponentClass;

struct _SunOneComponent {
	BonoboObject parent;
	
	SunOneComponentPrivate *priv;
};

struct _SunOneComponentClass {
	BonoboObjectClass parent_class;

	POA_GNOME_Evolution_Component__epv epv;
};

extern SunOneComponent *global_sunone_component;

GType            sunone_component_get_type (void);
SunOneComponent *sunone_component_new      (void);

SunOneAccount   *sunone_component_get_account_from_uri (SunOneComponent *component,
							   const char        *uri);
SunOneAccount   *sunone_component_get_account_from_uid (SunOneComponent *component,
							   const char        *uid);
GSList 		*sunone_component_get_account_list (SunOneComponent *component);
gboolean         sunone_component_is_interactive      (SunOneComponent *component);

void		 sunone_component_set_offline_listener (SunOneComponent *component, SunOneOfflineListener *listener);
void		 sunone_component_is_offline (SunOneComponent *component, int *state);

#define SUNONE_COMPONENT_FACTORY_IID  "OAFIID:GNOME_Evolution_SunOne_Component_Factory:" BASE_VERSION
#define SUNONE_COMPONENT_IID	"OAFIID:GNOME_Evolution_SunOne_Component:" BASE_VERSION

G_END_DECLS

#endif /* __SUNONE_COMPONENT_H__ */
