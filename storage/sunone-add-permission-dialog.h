/* sunone-add-permission-dialog.h
 *
 * Copyright (C) 2002-2004 Sun Microsystems, Inc
 *
 * AUTHORS
 *     Jack Jia <jack.jia@sun.com>
 *     Harry Lu <harry.lu@sun.com>
 *     Alfred Peng <alfred.peng@sun.com>
 *     Rodrigo Moya <rodrigo@ximian.com>
 *
 */

#ifndef SUNONE_ADD_PERMISSION_DIALOG_H
#define SUNONE_ADD_PERMISSION_DIALOG_H

#include "lib/sunone-ace.h"
#include "sunone-permissions-dialog.h"

G_BEGIN_DECLS

#define SUNONE_ADD_PERMISSION_DIALOG_TYPE            (sunone_add_permission_dialog_get_type ())
#define SUNONE_ADD_PERMISSION_DIALOG(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), SUNONE_ADD_PERMISSION_DIALOG_TYPE, SunOneAddPermissionDialog))
#define SUNONE_ADD_PERMISSION_DIALOG_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), SUNONE_ADD_PERMISSION_DIALOG_TYPE, SunOneAddPermissionDialogClass))
#define IS_SUNONE_ADD_PERMISSION_DIALOG(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), SUNONE_ADD_PERMISSION_DIALOG_TYPE))
#define IS_SUNONE_ADD_PERMISSION_DIALOG_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), SUNONE_ADD_PERMISSION_DIALOG_TYPE))

typedef struct _SunOneAddPermissionDialog		SunOneAddPermissionDialog;
typedef struct _SunOneAddPermissionDialogClass	SunOneAddPermissionDialogClass;
typedef struct _SunOneAddPermissionDialogPrivate      SunOneAddPermissionDialogPrivate;

struct _SunOneAddPermissionDialog {
	GtkDialog dialog;

	/* private data */
	SunOneAddPermissionDialogPrivate *priv;
};

struct _SunOneAddPermissionDialogClass {
	GtkDialogClass parent_class;
};

GType    sunone_add_permission_dialog_get_type (void);
GtkWidget *sunone_add_permission_dialog_new (SunOnePermissionsDialog *perm_dialog, SunOneACE *ace);

G_END_DECLS

#endif
