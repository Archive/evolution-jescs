/* sunone-sunone_itip-view.h
 *
 * Copyright (C) 2005  Sun Microsystems, Inc.
 *
 * AUTHORS
 *	Harry Lu <harry.lu@sun.com>
 */

#ifndef _SUNONE_ITIP_VIEW_H_
#define _SUNONE_ITIP_VIEW_H_

#include <gtk/gtkhbox.h>
#include <libecal/e-cal.h>

G_BEGIN_DECLS

#define SUNONE_ITIP_TYPE_VIEW            (sunone_itip_view_get_type ())
#define SUNONE_ITIP_VIEW(object)         (G_TYPE_CHECK_INSTANCE_CAST ((object), SUNONE_ITIP_TYPE_VIEW, SunOneItipView))
#define SUNONE_ITIP_VIEW_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), SUNONE_ITIP_TYPE_VIEW, SunOneItipViewClass))
#define SUNONE_ITIP_IS_VIEW(object)      (G_TYPE_CHECK_INSTANCE_TYPE ((object), SUNONE_ITIP_TYPE_VIEW))
#define SUNONE_ITIP_IS_VIEW_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), SUNONE_ITIP_TYPE_VIEW))
#define SUNONE_ITIP_VIEW_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), SUNONE_ITIP_TYPE_VIEW, SunOneItipViewClass))

typedef struct _SunOneItipView        SunOneItipView;
typedef struct _SunOneItipViewPrivate SunOneItipViewPrivate;
typedef struct _SunOneItipViewClass   SunOneItipViewClass;

typedef enum {
	SUNONE_ITIP_VIEW_MODE_NONE,
	SUNONE_ITIP_VIEW_MODE_PUBLISH,
	SUNONE_ITIP_VIEW_MODE_REQUEST,
	SUNONE_ITIP_VIEW_MODE_COUNTER,
	SUNONE_ITIP_VIEW_MODE_DECLINECOUNTER,
	SUNONE_ITIP_VIEW_MODE_ADD,
	SUNONE_ITIP_VIEW_MODE_REPLY,
	SUNONE_ITIP_VIEW_MODE_REFRESH,
	SUNONE_ITIP_VIEW_MODE_CANCEL
} SunOneItipViewMode;

typedef enum {
	SUNONE_ITIP_VIEW_RESPONSE_NONE,
	SUNONE_ITIP_VIEW_RESPONSE_ACCEPT,
	SUNONE_ITIP_VIEW_RESPONSE_TENTATIVE,
	SUNONE_ITIP_VIEW_RESPONSE_DECLINE,
	SUNONE_ITIP_VIEW_RESPONSE_UPDATE,
	SUNONE_ITIP_VIEW_RESPONSE_CANCEL,
	SUNONE_ITIP_VIEW_RESPONSE_REFRESH,
	SUNONE_ITIP_VIEW_RESPONSE_OPEN
} SunOneItipViewResponse;

typedef enum {
	SUNONE_ITIP_VIEW_INFO_ITEM_TYPE_NONE,
	SUNONE_ITIP_VIEW_INFO_ITEM_TYPE_INFO,
	SUNONE_ITIP_VIEW_INFO_ITEM_TYPE_WARNING,
	SUNONE_ITIP_VIEW_INFO_ITEM_TYPE_ERROR,
	SUNONE_ITIP_VIEW_INFO_ITEM_TYPE_PROGRESS
} SunOneItipViewInfoItemType;

struct _SunOneItipView {
	GtkHBox parent_instance;
	
	SunOneItipViewPrivate *priv;
};

struct _SunOneItipViewClass {
	GtkHBoxClass parent_class;
};

GType      sunone_itip_view_get_type (void);
GtkWidget *sunone_itip_view_new      (void);

void sunone_itip_view_set_mode (SunOneItipView *view, SunOneItipViewMode mode);
SunOneItipViewMode sunone_itip_view_get_mode (SunOneItipView *view);

void sunone_itip_view_set_item_type (SunOneItipView *view, ECalSourceType type);
ECalSourceType sunone_itip_view_get_item_type (SunOneItipView *view);

void sunone_itip_view_set_organizer (SunOneItipView *view, const char *organizer);
const char *sunone_itip_view_get_organizer (SunOneItipView *view);

void sunone_itip_view_set_sentby (SunOneItipView *view, const char *sentby);
const char *sunone_itip_view_get_sentby (SunOneItipView *view);

void sunone_itip_view_set_attendee (SunOneItipView *view, const char *attendee);
const char *sunone_itip_view_get_attendee (SunOneItipView *view);

void sunone_itip_view_set_delegator (SunOneItipView *view, const char *delegator);
const char *sunone_itip_view_get_delegator (SunOneItipView *view);

void sunone_itip_view_set_summary (SunOneItipView *view, const char *summary);
const char *sunone_itip_view_get_summary (SunOneItipView *view);

void sunone_itip_view_set_location (SunOneItipView *view, const char *location);
const char *sunone_itip_view_get_location (SunOneItipView *view);

void sunone_itip_view_set_status (SunOneItipView *view, const char *status);
const char *sunone_itip_view_get_status (SunOneItipView *view);

void sunone_itip_view_set_comment (SunOneItipView *view, const char *comment);
const char *sunone_itip_view_get_comment (SunOneItipView *view);

void sunone_itip_view_set_description (SunOneItipView *view, const char *description);
const char *sunone_itip_view_get_description (SunOneItipView *view);

void sunone_itip_view_set_start (SunOneItipView *view, struct tm *start);
const struct tm *sunone_itip_view_get_start (SunOneItipView *view);

void sunone_itip_view_set_end (SunOneItipView *view, struct tm *end);
const struct tm *sunone_itip_view_get_end (SunOneItipView *view);

guint sunone_itip_view_add_upper_info_item (SunOneItipView *view, SunOneItipViewInfoItemType type, const char *message); 
guint sunone_itip_view_add_upper_info_item_printf (SunOneItipView *view, SunOneItipViewInfoItemType, const char *format, ...) G_GNUC_PRINTF (3, 4);
void sunone_itip_view_remove_upper_info_item (SunOneItipView *view, guint id);
void sunone_itip_view_clear_upper_info_items (SunOneItipView *view);

guint sunone_itip_view_add_lower_info_item (SunOneItipView *view, SunOneItipViewInfoItemType type, const char *message); 
guint sunone_itip_view_add_lower_info_item_printf (SunOneItipView *view, SunOneItipViewInfoItemType type, const char *format, ...) G_GNUC_PRINTF (3, 4);
void sunone_itip_view_remove_lower_info_item (SunOneItipView *view, guint id);
void sunone_itip_view_clear_lower_info_items (SunOneItipView *view);

void sunone_itip_view_set_rsvp (SunOneItipView *view, gboolean rsvp);
gboolean sunone_itip_view_get_rsvp (SunOneItipView *view);

void sunone_itip_view_set_show_rsvp (SunOneItipView *view, gboolean rsvp);
gboolean sunone_itip_view_get_show_rsvp (SunOneItipView *view);

void sunone_itip_view_set_rsvp_comment (SunOneItipView *view, const char *comment);
const char *sunone_itip_view_get_rsvp_comment (SunOneItipView *view);

G_END_DECLS

#endif
