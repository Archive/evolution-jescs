/* sunone-config-listener.h
 *
 * Copyright (C) 2002-2004 Sun Microsystems, Inc
 *
 * AUTHORS
 *     Jack Jia <jack.jia@sun.com>
 *     Harry Lu <harry.lu@sun.com>
 *     Alfred Peng <alfred.peng@sun.com>
 *     Jedy Wang <jedy.wang@sun.com>
 *
 */

#ifndef SUNONE_CONFIG_LISTENER_H
#define SUNONE_CONFIG_LISTENER_H

#include <glib-object.h>
#include <libedataserver/e-account-list.h>
#include <libedataserver/e-source.h>
#include <libedataserver/e-source-list.h>
#include "lib/sunone-connection.h"
#include "sunone-account.h"
                         
G_BEGIN_DECLS


#define SUNONE_TYPE_CONFIG_LISTENER           (sunone_config_listener_get_type ())
#define SUNONE_CONFIG_LISTENER(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), SUNONE_TYPE_CONFIG_LISTENER, SunOneConfigListener))
#define SUNONE_CONFIG_LISTENER_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), SUNONE_TYPE_CONFIG_LISTENER,  SunOneConfigListenerClass))
#define SUNONE_IS_CONFIG_LISTENER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), SUNONE_TYPE_CONFIG_LISTENER))
#define SUNONE_IS_CONFIG_LISTENER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((obj), SUNONE_TYPE_CONFIG_LISTENER))

typedef struct _SunOneConfigListener SunOneConfigListener;
typedef struct _SunOneConfigListenerClass SunOneConfigListenerClass;
typedef struct _SunOneConfigListenerPrivate SunOneConfigListenerPrivate;

struct _SunOneConfigListener {
       EAccountList parent;

       SunOneConfigListenerPrivate *priv;
};
                                                                                                                             
struct _SunOneConfigListenerClass {
       EAccountListClass parent_class;

	   /* signal */
	   void (*sunone_account_created) (SunOneConfigListener *, SunOneAccount *);
	   void (*sunone_account_removed) (SunOneConfigListener *, SunOneAccount *);
	   void (*sunone_account_changed) (SunOneConfigListener *, SunOneAccount *);
};
                                                                                                                             
GType                sunone_config_listener_get_type (void);
SunOneConfigListener *sunone_config_listener_new (void);

GSList	*sunone_config_listener_get_accounts (SunOneConfigListener *config_listener);
gboolean	sunone_config_listener_construct_account (SunOneConfigListener *config_listener);

void	sunone_folder_tree_sync (void);
void    remove_source (SunOneAccount *account, const char *calid, gboolean is_account);
void	add_sources (SunOneAccount *account, SunOneConnectionPreferences *prefs);
void	add_source (SunOneAccount *account, const char *calid,
			const char *display_name, gboolean personal);
void	rename_source (SunOneAccount *account, const char *calid, const char *display_name,
			gboolean is_account);

G_END_DECLS
                                                                                                                             
#endif 

