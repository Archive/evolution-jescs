/* sunone-backend-view.c
 *
 * Copyright (C) 2002-2005 Sun Microsystems, Inc
 *
 * AUTHORS
 *     Harry Lu <harry.lu@sun.com>
 *     Alfred Peng <alfred.peng@sun.com>
 *
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <string.h>

#include <gtk/gtk.h>
#include <shell/e-user-creatable-items-handler.h>

#include "sunone-backend-view.h"
#include "sunone-account.h"
#include "sunone-config-listener.h"
#include "sunone-component.h"
#include "sunone-folder-tree.h"

#define d(x)

static GObjectClass *parent_class = NULL;

struct _SunOneBackendViewPrivate {
	SunOneConfigListener *config_listener;

	SunOneFolderTree *sidebar;
	BonoboControl *sidebar_control;

	GtkWidget *statusbar;
	BonoboControl *statusbar_control;

	BonoboControl *view_control;

	EUserCreatableItemsHandler *items_handler;
};

G_DEFINE_TYPE (SunOneBackendView, sunone_backend_view, G_TYPE_OBJECT)

static void
dispose (GObject *object)
{
	SunOneBackendView *view = SUNONE_BACKEND_VIEW (object);
	SunOneBackendViewPrivate *priv = view->priv;

	if (priv) {
		if (priv->config_listener) {
			g_object_unref (priv->config_listener);
			priv->config_listener = NULL;
		}

		if (priv->sidebar) {
			g_object_unref (priv->sidebar);
			priv->sidebar = NULL;
		}

		g_free (priv);
		view->priv = NULL;
	}

	G_OBJECT_CLASS (parent_class)->dispose (object);
}

static void
finalize (GObject *object)
{
	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
activated (BonoboControl *control, gboolean active, gpointer user_data)
{
	SunOneBackendView *view = user_data;
	BonoboUIComponent *uic;

	uic = bonobo_control_get_ui_component (control);
	g_return_if_fail (uic != NULL);

	if (active) {
		Bonobo_UIContainer remote_uih;

		remote_uih  = bonobo_control_get_remote_ui_container (control, NULL);
		bonobo_ui_component_set_container (uic, remote_uih, NULL);
		bonobo_object_release_unref (remote_uih, NULL);

		e_user_creatable_items_handler_activate (view->priv->items_handler, uic);
	} else {
		 bonobo_ui_component_unset_container (uic, NULL);
	}
}

static void
sunone_backend_view_class_init (SunOneBackendViewClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->dispose = dispose;
	object_class->finalize = finalize;
}

static void
sunone_backend_view_init (SunOneBackendView *backend)
{
	SunOneBackendViewPrivate *priv;

	backend->priv = g_new0 (SunOneBackendViewPrivate, 1);
	priv = backend->priv;
}


static void
sunone_account_created (SunOneConfigListener *config_listener,
			  SunOneAccount *account, gpointer user_data)
{
	SunOneBackendView *view = user_data;
	SunOneBackendViewPrivate *priv = view->priv;

	sunone_folder_tree_created (priv->sidebar, account);
}

static void
sunone_account_removed (SunOneConfigListener *config_listener,
			  SunOneAccount *account, gpointer user_data) 
{
	SunOneBackendView *view = user_data;

	sunone_folder_tree_removed (view->priv->sidebar, account);
	sunone_folder_tree_sync ();
}

static void
sunone_account_changed (SunOneConfigListener *config_listener,
			  SunOneAccount *account, gpointer user_data) 
{
	SunOneBackendView *view = user_data;
	SunOneAccount *old_account;

	old_account =  sunone_component_get_account_from_uid (global_sunone_component,
									sunone_account_get_uid (account));
	if (old_account) {
		sunone_folder_tree_removed (view->priv->sidebar, old_account);
		g_object_unref (old_account);

		sunone_account_set_name (old_account,
					sunone_account_get_name (account));
		sunone_account_set_uid (old_account,
					sunone_account_get_uid (account));
		sunone_account_set_server (old_account,
					sunone_account_get_server (account));
		sunone_account_set_protocol (old_account,
					sunone_account_get_protocol (account));
		sunone_account_set_user (old_account,
					sunone_account_get_user (account));
		sunone_account_set_email (old_account,
					sunone_account_get_email (account));
		sunone_account_set_proxy (old_account,
					sunone_account_get_proxy (account));
		sunone_account_set_poll_interval (old_account,
					sunone_account_get_poll_interval (account));
		sunone_folder_tree_created (view->priv->sidebar, old_account);
	}

}

SunOneBackendView *
sunone_backend_view_new (SunOneConfigListener *config_listener)
{
	SunOneBackendView *view;
	SunOneBackendViewPrivate *priv;
	SunOneFolderTree *tree;
	GSList *accounts, *acc;

	view = g_object_new (SUNONE_TYPE_BACKEND_VIEW, NULL);
	priv = view->priv;

	priv->config_listener = g_object_ref (config_listener);

	g_signal_connect (config_listener, "sunone_account_created",
			  G_CALLBACK (sunone_account_created), view);
	g_signal_connect (config_listener, "sunone_account_removed",
			  G_CALLBACK (sunone_account_removed), view);
	g_signal_connect (config_listener, "sunone_account_changed",
			  G_CALLBACK (sunone_account_changed), view);

	tree = sunone_folder_tree_new ();
	priv->sidebar = tree;
	priv->sidebar_control = bonobo_control_new (sunone_folder_tree_get_scrolled (tree));

	priv->view_control = bonobo_control_new ((GtkWidget *)sunone_folder_tree_get_body (tree));
	g_signal_connect (priv->view_control, "activate",
			  G_CALLBACK (activated), view);

	accounts = sunone_component_get_account_list (global_sunone_component);
	for (acc = accounts; acc; acc = acc->next)
		sunone_folder_tree_created (tree, acc->data);

	priv->statusbar = gtk_drawing_area_new ();
	gtk_widget_show (priv->statusbar);
	priv->statusbar_control = bonobo_control_new (priv->statusbar);

	priv->items_handler = e_user_creatable_items_handler_new ("JESCS", NULL, NULL);

	return view;
}

BonoboControl *
sunone_backend_view_get_sidebar (SunOneBackendView *view)
{
	g_return_val_if_fail (SUNONE_IS_BACKEND_VIEW (view), NULL);

	return view->priv->sidebar_control;
}

BonoboControl *
sunone_backend_view_get_view (SunOneBackendView *view)
{
	g_return_val_if_fail (SUNONE_IS_BACKEND_VIEW (view), NULL);

	return view->priv->view_control;
}

BonoboControl *
sunone_backend_view_get_statusbar (SunOneBackendView *view)
{
	g_return_val_if_fail (SUNONE_IS_BACKEND_VIEW (view), NULL);

	return view->priv->statusbar_control;
}
