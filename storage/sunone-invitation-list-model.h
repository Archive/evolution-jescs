/* sunone-invitation-list-model.h
 *
 * Copyright (C) 2002-2005 Sun Microsystems, Inc
 *
 * AUTHORS
 *     Jack Jia <jack.jia@sun.com>
 *     Harry Lu <harry.lu@sun.com>
 *     Alfred Peng <alfred.peng@sun.com>
 *     Rodrigo Moya <rodrigo@ximian.com>
 *
 */

#ifndef SUNONE_INVITATION_LIST_MODEL_H
#define SUNONE_INVITATION_LIST_MODEL_H

#include <gtk/gtk.h>
#include <bonobo/bonobo-generic-factory.h>
#include <glade/glade-xml.h>
#include <libecal/e-cal-component.h>
#include "lib/sunone-connection.h"
#include "sunone-account.h"

G_BEGIN_DECLS

enum {
	START_COL,
	END_COL,
	LOCATION_COL,
	SUMMARY_COL,
	ORGANIZER_COL,
	STATUS_COL,
	LAST_COL
};

#define SUNONE_TYPE_INVITATION_LIST_MODEL            (sunone_invitation_list_model_get_type ())
#define SUNONE_INVITATION_LIST_MODEL(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), SUNONE_TYPE_INVITATION_LIST_MODEL, SunOneInvitationListModel))
#define SUNONE_INVITATION_LIST_MODEL_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), SUNONE_TYPE_INVITATION_LIST_MODEL, SunOneInvitationListModelClass))
#define SUNONE_IS_INVITATION_LIST_MODEL(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), SUNONE_TYPE_INVITATION_LIST_MODEL))
#define SUNONE_IS_INVITATION_LIST_MODEL_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), SUNONE_TYPE_INVITATION_LIST_MODEL))

typedef struct _SunOneInvitationListModel		SunOneInvitationListModel;
typedef struct _SunOneInvitationListModelClass		SunOneInvitationListModelClass;
typedef struct _SunOneInvitationListModelPrivate      SunOneInvitationListModelPrivate;

struct _SunOneInvitationListModel {
	GtkListStore parent;
	SunOneCalendarProperties *props;
	SunOneAccount *account;
	icaltimezone *zone;

	/* private data */
	SunOneInvitationListModelPrivate *priv;
#if 0
	gint stamp;
	char *uri;
	char *calid;
	SunOneConnection *so_cnc;
	
	GHashTable *invitations;
	GList *invitations_list;

	EConfigListener *config;
	
	gint timeout_id;
#endif
};

struct _SunOneInvitationListModelClass {
	GtkListStoreClass parent_class;
};

GType		sunone_invitation_list_model_get_type (void);
void		sunone_invitation_list_model_set_value (SunOneInvitationListModel *model, int col, int row, const char *val);
const char		*sunone_invitation_list_model_get_uri (SunOneInvitationListModel *model);
gboolean		sunone_invitation_list_model_poll_cb (gpointer data);
ECalComponent	*sunone_invitation_list_model_get_comp (SunOneInvitationListModel *model, int row);
SunOneInvitationListModel	*sunone_invitation_list_model_new (const char *uri);


G_END_DECLS

#endif
