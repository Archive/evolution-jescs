/* sunone-account.h
 *
 * Copyright (C) 2002-2005 Sun Microsystems, Inc
 *
 * AUTHORS
 *     Jack Jia <jack.jia@sun.com>
 *     Harry Lu <harry.lu@sun.com>
 *     Alfred Peng <alfred.peng@sun.com>
 *     Jedy Wang <jedy.wang@sun.com>
 *     Rodrigo Moya <rodrigo@ximian.com>
 *
 */

#ifndef SUNONE_ACCOUNT_H
#define SUNONE_ACCOUNT_H

#include <libedataserver/e-account-list.h>
#include "lib/sunone-connection.h"

G_BEGIN_DECLS

#define SUNONE_ACCOUNT_TYPE            (sunone_account_get_type ())
#define SUNONE_ACCOUNT(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), SUNONE_ACCOUNT_TYPE, SunOneAccount))
#define SUNONE_ACCOUNT_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), SUNONE_ACCOUNT_TYPE, SunOneAccountClass))
#define IS_SUNONE_ACCOUNT(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), SUNONE_ACCOUNT_TYPE))
#define IS_SUNONE_ACCOUNT_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), SUNONE_ACCOUNT_TYPE))
#define SUNONE_ACCOUNT_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), SUNONE_ACCOUNT_TYPE, SunOneAccountClass))

typedef struct _SunOneAccount      SunOneAccount;
typedef struct _SunOneAccountClass SunOneAccountClass;
typedef struct _SunOneAccountPrivate      SunOneAccountPrivate;

struct _SunOneAccount {
	GObject object;

	/* private data */
	SunOneAccountPrivate *priv;
};

struct _SunOneAccountClass {
	GObjectClass parent_class;
};

typedef enum {
	SUNONE_ACCOUNT_RESULT_SUCCESS,
	SUNONE_ACCOUNT_RESULT_DUPLICATED_NAME,
	SUNONE_ACCOUNT_RESULT_INVALID_DATA
} SunOneAccountResult;

GType             sunone_account_get_type (void);

const char 		   *sunone_account_get_email (SunOneAccount *account);
void 		   		sunone_account_set_email (SunOneAccount *account, const char *id_email);
const char         *sunone_account_get_name (SunOneAccount *account);
void                sunone_account_set_name (SunOneAccount *account, const char *name);
const char         *sunone_account_get_uid (SunOneAccount *account);
void                sunone_account_set_uid (SunOneAccount *account, const char *uid);
const char         *sunone_account_get_server (SunOneAccount *account);
void                sunone_account_set_server (SunOneAccount *account, const char *server);
const char         *sunone_account_get_protocol (SunOneAccount *account);
void                sunone_account_set_protocol (SunOneAccount *account, const char *protocol);
const char         *sunone_account_get_user (SunOneAccount *account);
void                sunone_account_set_user (SunOneAccount *account, const char *user);
const char         *sunone_account_get_password (SunOneAccount *account, gboolean is_interactive, const char *prompt);
void                sunone_account_set_proxy (SunOneAccount *account, const char *proxy);
const char         *sunone_account_get_proxy (SunOneAccount *account);
void                sunone_account_set_password (SunOneAccount *account, const char *password);
int		    		sunone_account_get_poll_interval (SunOneAccount *account);
void		    	sunone_account_set_poll_interval (SunOneAccount *account, int poll_interval);
gboolean            sunone_account_get_save_password (SunOneAccount *account);
void                sunone_account_set_save_password (SunOneAccount *account, gboolean save_password);
gboolean			sunone_account_get_dont_ask_password (SunOneAccount *account);
void				sunone_account_set_dont_ask_password (SunOneAccount *account, gboolean dont_ask_password);
void				sunone_account_set_connecting (SunOneAccount *account, gboolean status);
gboolean			sunone_account_is_connecting (SunOneAccount *account);

void                sunone_account_forget_password (SunOneAccount *account);
SunOneAccount	   *sunone_account_new (EAccountList *account_list, EAccount *account);

gboolean	    sunone_account_set_offline (SunOneAccount *account);
gboolean	    sunone_account_set_online (SunOneAccount *account);
void		    sunone_account_is_offline (SunOneAccount *account, int *mode);

SunOneConnection *  sunone_account_get_connection (SunOneAccount *account);
char			*sunone_account_get_physical_uri (SunOneAccount *account, const char *calid, const char *type);

G_END_DECLS

#endif
