/* sunone-offline-listener.c
 *
 * Copyright (C) 2005 Sun Microsystems, Inc
 *
 * AUTHORS
 *	Harry Lu <harry.lu@sun.com>
 *
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "sunone-offline-listener.h"
#include <libedata-cal/e-data-cal-factory.h>
#include <gconf/gconf-client.h>

static GObjectClass *parent_class = NULL;

struct _SunOneOfflineListenerPrivate 
{
	GConfClient *default_client;
	EDataCalFactory *cal_factory;
	gboolean is_offline_now;
};


static void 
set_online_status (SunOneOfflineListener *sunone_offline_listener, gboolean is_offline)
{
	SunOneOfflineListenerPrivate *priv;
	
	priv = sunone_offline_listener->priv;
	
	if (is_offline) {
		e_data_cal_factory_set_backend_mode (priv->cal_factory, OFFLINE_MODE);
	} else {
		e_data_cal_factory_set_backend_mode (priv->cal_factory, ONLINE_MODE);
	}

}
static void 
online_status_changed (GConfClient *client, int cnxn_id, GConfEntry *entry, gpointer data)
{
	GConfValue *value;
	gboolean sunone_offline;
        SunOneOfflineListener *sunone_offline_listener;
	SunOneOfflineListenerPrivate *priv;

	sunone_offline_listener = SUNONE_OFFLINE_LISTENER(data);
	priv = sunone_offline_listener->priv;
	sunone_offline = FALSE;
	value = gconf_entry_get_value (entry);
	if (value)
		sunone_offline = gconf_value_get_bool (value);
	if (priv->is_offline_now != sunone_offline) {
		priv->is_offline_now = sunone_offline;
		set_online_status (sunone_offline_listener ,sunone_offline);
	}
	
}


static void 
setup_sunone_offline_listener (SunOneOfflineListener *sunone_offline_listener)
{
	SunOneOfflineListenerPrivate *priv = sunone_offline_listener->priv;
	
	priv->default_client = gconf_client_get_default ();
	gconf_client_add_dir (priv->default_client, "/apps/evolution/shell", GCONF_CLIENT_PRELOAD_RECURSIVE,NULL);
	gconf_client_notify_add (priv->default_client, "/apps/evolution/shell/start_offline", (GConfClientNotifyFunc)online_status_changed, sunone_offline_listener, NULL, NULL);
	priv->is_offline_now = gconf_client_get_bool (priv->default_client, "/apps/evolution/shell/start_offline", NULL);
	set_online_status (sunone_offline_listener, priv->is_offline_now); 
}

SunOneOfflineListener*
sunone_offline_listener_new (EDataCalFactory *cal_factory)
{
	SunOneOfflineListener *sunone_offline_listener = g_object_new (SUNONE_OFFLINE_TYPE_LISTENER, NULL);
	SunOneOfflineListenerPrivate *priv = sunone_offline_listener->priv;
	
	priv->cal_factory = cal_factory;
	setup_sunone_offline_listener (sunone_offline_listener);
	return sunone_offline_listener;

}

void
sunone_is_offline (SunOneOfflineListener *offline_listener, int *state)
{
	SunOneOfflineListenerPrivate * priv;

	g_return_if_fail (SUNONE_OFFLINE_IS_LISTENER (offline_listener));

	priv = offline_listener->priv;
	
	if (priv->is_offline_now)
		*state = OFFLINE_MODE;
	else
		*state = ONLINE_MODE;
}

static void
sunone_offline_listener_dispose (GObject *object)
{
	SunOneOfflineListener *sunone_offline_listener = SUNONE_OFFLINE_LISTENER (object);
	if (sunone_offline_listener->priv->default_client) {
		g_object_unref (sunone_offline_listener->priv->default_client);
		sunone_offline_listener->priv->default_client = NULL;
	}
	(* G_OBJECT_CLASS (parent_class)->dispose) (object);
}

static void
sunone_offline_listener_finalize (GObject *object)
{
	SunOneOfflineListener *sunone_offline_listener;
	SunOneOfflineListenerPrivate *priv;

	sunone_offline_listener = SUNONE_OFFLINE_LISTENER (object);
	priv = sunone_offline_listener->priv;
	
	g_free (priv);
	sunone_offline_listener->priv = NULL;
	
	parent_class->finalize (object);
}

static void
sunone_offline_listener_init (SunOneOfflineListener *listener)
{
	SunOneOfflineListenerPrivate *priv;
	
	priv =g_new0 (SunOneOfflineListenerPrivate, 1);
	listener->priv = priv;
	
}



static void
sunone_offline_listener_class_init (SunOneOfflineListener *klass)
{
	GObjectClass *object_class;

	parent_class = g_type_class_peek_parent (klass);

	object_class = G_OBJECT_CLASS (klass);
	object_class->dispose = sunone_offline_listener_dispose;
	object_class->finalize = sunone_offline_listener_finalize;
}


GType
sunone_offline_listener_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static GTypeInfo info = {
                        sizeof (SunOneOfflineListenerClass),
                        (GBaseInitFunc) NULL,
                        (GBaseFinalizeFunc) NULL,
                        (GClassInitFunc) sunone_offline_listener_class_init,
                        NULL, NULL,
                        sizeof (SunOneOfflineListener),
                        0,
                        (GInstanceInitFunc) sunone_offline_listener_init,
                };
		type = g_type_register_static (G_TYPE_OBJECT, "SunOneOfflineListener", &info, 0);
	}

	return type;
}
