/* sunone-subscription-dialog.h
 *
 * Copyright (C) 2002-2004 Sun Microsystems, Inc
 *
 * AUTHORS
 *     Jack Jia <jack.jia@sun.com>
 *     Harry Lu <harry.lu@sun.com>
 *     Alfred Peng <alfred.peng@sun.com>
 *     Rodrigo Moya <rodrigo@ximian.com>
 *
 */

#ifndef SUNONE_SUBSCRIPTION_DIALOG_H
#define SUNONE_SUBSCRIPTION_DIALOG_H

#include <gtk/gtkdialog.h>
#include <glade/glade-xml.h>
#include "sunone-connection.h"
#include "sunone-folder-tree.h"

G_BEGIN_DECLS

#define SUNONE_SUBSCRIPTION_DIALOG_TYPE            (sunone_subscription_dialog_get_type ())
#define SUNONE_SUBSCRIPTION_DIALOG(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), SUNONE_SUBSCRIPTION_DIALOG_TYPE, SunOneSubscriptionDialog))
#define SUNONE_SUBSCRIPTION_DIALOG_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), SUNONE_SUBSCRIPTION_DIALOG_TYPE, SunOneSubscriptionDialogClass))
#define IS_SUNONE_SUBSCRIPTION_DIALOG(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), SUNONE_SUBSCRIPTION_DIALOG_TYPE))
#define IS_SUNONE_SUBSCRIPTION_DIALOG_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), SUNONE_SUBSCRIPTION_DIALOG_TYPE))

typedef struct _SunOneSubscriptionDialog		SunOneSubscriptionDialog;
typedef struct _SunOneSubscriptionDialogClass		SunOneSubscriptionDialogClass;
typedef struct _SunOneSubscriptionDialogPrivate      SunOneSubscriptionDialogPrivate;

enum {
	SUB_COLUMN,
	SUB_CALID_COLUMN,
	SUB_OWNER_COLUMN,
	SUB_DES_COLUMN,
	SUB_TREE_N_COLUMNS
};

struct _SunOneSubscriptionDialog {
	GtkDialog dialog;

	/* private data */
	SunOneSubscriptionDialogPrivate *priv;
};

struct _SunOneSubscriptionDialogClass {
	GtkDialogClass parent_class;
};

GType    sunone_subscription_dialog_get_type (void);
GtkWidget *sunone_subscription_dialog_new (SunOneFolderTree *tree, SunOneConnection *cnc);

G_END_DECLS

#endif
