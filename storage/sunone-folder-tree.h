/* sunone-folder-tree.h
 *
 * Copyright (C) 2002-2004 Sun Microsystems, Inc
 *
 * AUTHORS
 *     Jack Jia <jack.jia@sun.com>
 *     Harry Lu <harry.lu@sun.com>
 *     Alfred Peng <alfred.peng@sun.com>
 *     Jedy Wang <jedy.wang@sun.com>
 *
 */

#ifndef __SUNONE_FOLDER_TREE_H__
#define __SUNONE_FOLDER_TREE_H__

#include <gtk/gtk.h>
#include "sunone-util.h"
#include "sunone-config-listener.h"
#include "sunone-account.h"
#include "sunone-invitation-list-model.h"

G_BEGIN_DECLS

#define SUNONE_TYPE_FOLDER_TREE               (sunone_folder_tree_get_type ())
#define SUNONE_FOLDER_TREE(obj)               (G_TYPE_CHECK_INSTANCE_CAST ((obj), SUNONE_TYPE_FOLDER_TREE, SunOneFolderTree))
#define SUNONE_FOLDER_TREE_CLASS(klass)       (G_TYPE_CHECK_CLASS_CAST ((klass), SUNONE_TYPE_FOLDER_TREE, SunOneFolderTreeClass))
#define SUNONE_IS_FOLDER_TREE(obj)            (G_TYPE_CHECK_INSTANCE_TYPE ((obj), SUNONE_TYPE_FOLDER_TREE))
#define SUNONE_IS_FOLDER_TREE_CLASS(klass)    (G_TYPE_CHECK_CLASS_TYPE ((klass), SUNONE_TYPE_FOLDER_TREE))

typedef struct  _SunOneFolderTree                    SunOneFolderTree;
typedef struct  _SunOneFolderTreePrivate             SunOneFolderTreePrivate;
typedef struct  _SunOneFolderTreeClass               SunOneFolderTreeClass;

enum {
	COL_STRING_DISPLAY_NAME,
	COL_POINTER_ACCOUNT,
	COL_STRING_FOLDER_TYPE,
	COL_STRING_URI,
	COL_STRING_FULLNAME,
	COL_BOOL_IS_STORE,
	COL_BOOL_IS_ACCOUNT,
	COL_BOOL_IS_PERSONAL,
	NUM_COLUMNS
};

enum _ej_popup_target_folder_t {
	EJ_POPUP_FOLDER_LOGIN = 1<<0,
	EJ_POPUP_FOLDER_OP = 1<<1,
	EJ_POPUP_FOLDER_PERM = 1<<2,
	EJ_POPUP_FOLDER_SUB = 1<<3, 
	EJ_POPUP_FOLDER_NEW_CALENDAR = 1<<4
};

struct _SunOneFolderTree {
	GObject parent;

	SunOneFolderTreePrivate *priv;
};

struct _SunOneFolderTreeClass {
	GObjectClass parent_class;

};

GType            sunone_folder_tree_get_type             (void);

SunOneFolderTree   *sunone_folder_tree_new 				  (void);
SunOneFolderTree   *sunone_folder_tree_new_with_model 	  (GtkTreeModel *model);

gboolean		 sunone_folder_tree_get_selected	 	  (SunOneFolderTree *tree);
GtkWidget		*sunone_folder_tree_get_scrolled		  (SunOneFolderTree *tree);
GtkNotebook		*sunone_folder_tree_get_body			  (SunOneFolderTree *tree);
GtkTreeView 	*sunone_folder_tree_get_tree_view		  (SunOneFolderTree *tree);
GtkTreeModel	*sunone_folder_tree_get_model			  (SunOneFolderTree *tree);
SunOneAccount	*sunone_folder_tree_get_account		  (SunOneFolderTree *tree);

void			 sunone_folder_tree_created 			  (SunOneFolderTree *tree, SunOneAccount *account);
void			 sunone_folder_tree_removed 			  (SunOneFolderTree *tree, SunOneAccount *account);
void			 sunone_folder_tree_add_calendar 		  (SunOneFolderTree *tree, SunOneAccount *account, const char *calid, const char *display_name);
void			 sunone_folder_tree_remove_calendar	  (SunOneFolderTree *tree, SunOneAccount *account, const char *calid);
void			 sunone_folder_tree_rename_calendar 		  (SunOneFolderTree *tree, SunOneAccount *account, const char *calid, const char *display_name);

SunOneInvitationListModel *sunone_folder_tree_get_invitation_list_model (SunOneFolderTree *tree, const char *physical_uri);

G_END_DECLS

#endif /* __SUNONE_FOLDER_TREE_H__ */
