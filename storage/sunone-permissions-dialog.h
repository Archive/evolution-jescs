/* sunone-permissions-dialog.h
 *
 * Copyright (C) 2002-2004 Sun Microsystems, Inc
 *
 * AUTHORS
 *     Jack Jia <jack.jia@sun.com>
 *     Harry Lu <harry.lu@sun.com>
 *     Alfred Peng <alfred.peng@sun.com>
 *     Rodrigo Moya <rodrigo@ximian.com>
 *
 */

#ifndef SUNONE_PERMISSIONS_DIALOG_H
#define SUNONE_PERMISSIONS_DIALOG_H

#include <gtk/gtkdialog.h>
#include <gtk/gtktreeview.h>
#include <glade/glade-xml.h>
#include "lib/sunone-connection.h"

G_BEGIN_DECLS

#define SUNONE_PERMISSIONS_DIALOG_TYPE            (sunone_permissions_dialog_get_type ())
#define SUNONE_PERMISSIONS_DIALOG(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), SUNONE_PERMISSIONS_DIALOG_TYPE, SunOnePermissionsDialog))
#define SUNONE_PERMISSIONS_DIALOG_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), SUNONE_PERMISSIONS_DIALOG_TYPE, SunOnePermissionsDialogClass))
#define IS_SUNONE_PERMISSIONS_DIALOG(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), SUNONE_PERMISSIONS_DIALOG_TYPE))
#define IS_SUNONE_PERMISSIONS_DIALOG_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), SUNONE_PERMISSIONS_DIALOG_TYPE))

enum {
	OWNERS_NAME_COLUMN,
	OWNERS_TREE_N_COLUMNS
};

enum {
	PERM_NAME_COLUMN,
	PERM_AVAILABILITY_COLUMN,
	PERM_INVITE_COLUMN,
	PERM_READ_COLUMN,
	PERM_DELETE_COLUMN,
	PERM_MODIFY_COLUMN,
	PERM_TREE_N_COLUMNS
};

typedef struct _SunOnePermissionsDialog		SunOnePermissionsDialog;
typedef struct _SunOnePermissionsDialogClass	SunOnePermissionsDialogClass;
typedef struct _SunOnePermissionsDialogPrivate	SunOnePermissionsDialogPrivate;

struct _SunOnePermissionsDialog {
	GtkDialog dialog;
	GList *merged_acls;

	/* private data */
	SunOnePermissionsDialogPrivate *priv;
};

struct _SunOnePermissionsDialogClass {
	GtkDialogClass parent_class;
};

GType    sunone_permissions_dialog_get_type (void);
GtkWidget *sunone_permissions_dialog_new (SunOneConnection *cnc, const gchar *calid);
GtkTreeView *sunone_permissions_dialog_get_permissions_tree_view(SunOnePermissionsDialog *dialog);


G_END_DECLS

#endif
