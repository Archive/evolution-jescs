/* sunone-invitation-list.h
 *
 * Copyright (C) 2002-2004 Sun Microsystems, Inc
 *
 * AUTHORS
 *     Jack Jia <jack.jia@sun.com>
 *     Harry Lu <harry.lu@sun.com>
 *     Alfred Peng <alfred.peng@sun.com>
 *     Rodrigo Moya <rodrigo@ximian.com>
 *
 */

#ifndef SUNONE_INVITATION_LIST_H
#define SUNONE_INVITATION_LIST_H

#include <gtk/gtkvbox.h>
#include <bonobo/bonobo-generic-factory.h>
#include <glade/glade-xml.h>
#include <lib/sunone-connection.h>
#include "sunone-invitation-list-model.h"

G_BEGIN_DECLS

#define SUNONE_INVITATION_LIST_TYPE            (sunone_invitation_list_get_type ())
#define SUNONE_INVITATION_LIST(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), SUNONE_INVITATION_LIST_TYPE, SunOneInvitationList))
#define SUNONE_INVITATION_LIST_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), SUNONE_INVITATION_LIST_TYPE, SunOneInvitationListClass))
#define IS_SUNONE_INVITATION_LIST(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), SUNONE_INVITATION_LIST_TYPE))
#define IS_SUNONE_INVITATION_LIST_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), SUNONE_INVITATION_LIST_TYPE))

typedef struct _SunOneInvitationList		SunOneInvitationList;
typedef struct _SunOneInvitationListClass		SunOneInvitationListClass;
typedef struct _SunOneInvitationListPrivate		SunOneInvitationListPrivate;

struct _SunOneInvitationList {
	GtkVBox parent;

	/* private data */
	SunOneInvitationListPrivate *priv;
};

struct _SunOneInvitationListClass {
	GtkVBoxClass parent_class;
};

GType        sunone_invitation_list_get_type (void);
GtkWidget *sunone_invitation_list_new      (SunOneInvitationListModel *model);

G_END_DECLS

#endif
