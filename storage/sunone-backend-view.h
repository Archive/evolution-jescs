/* sunone-backend-view.h
 *
 * Copyright (C) 2002-2004 Sun Microsystems, Inc
 *
 * AUTHORS
 *     Jack Jia <jack.jia@sun.com>
 *     Harry Lu <harry.lu@sun.com>
 *     Alfred Peng <alfred.peng@sun.com>
 *
 */

#ifndef __SUNONE_BACKEND_VIEW_H__
#define __SUNONE_BACKEND_VIEW_H__

#include <bonobo/bonobo-control.h>
#include "sunone-util.h"
#include "sunone-config-listener.h"

G_BEGIN_DECLS

#define SUNONE_TYPE_BACKEND_VIEW               (sunone_backend_view_get_type ())
#define SUNONE_BACKEND_VIEW(obj)               (G_TYPE_CHECK_INSTANCE_CAST ((obj), SUNONE_TYPE_BACKEND_VIEW, SunOneBackendView))
#define SUNONE_BACKEND_VIEW_CLASS(klass)       (G_TYPE_CHECK_CLASS_CAST ((klass), SUNONE_TYPE_BACKEND_VIEW, SunOneBackendViewClass))
#define SUNONE_IS_BACKEND_VIEW(obj)            (G_TYPE_CHECK_INSTANCE_TYPE ((obj), SUNONE_TYPE_BACKEND_VIEW))
#define SUNONE_IS_BACKEND_VIEW_CLASS(klass)    (G_TYPE_CHECK_CLASS_TYPE ((klass), SUNONE_TYPE_BACKEND_VIEW))

typedef struct  _SunOneBackendView                    SunOneBackendView;
typedef struct  _SunOneBackendViewPrivate             SunOneBackendViewPrivate;
typedef struct  _SunOneBackendViewClass               SunOneBackendViewClass;


struct _SunOneBackendView {
	GObject parent;

	SunOneBackendViewPrivate *priv;
};

struct _SunOneBackendViewClass {
	GObjectClass parent_class;

};

GType            sunone_backend_view_get_type             (void);

SunOneBackendView   *sunone_backend_view_new                  (SunOneConfigListener *config_listener);

BonoboControl   *sunone_backend_view_get_sidebar          (SunOneBackendView *view);
BonoboControl   *sunone_backend_view_get_view             (SunOneBackendView *view);
BonoboControl   *sunone_backend_view_get_statusbar        (SunOneBackendView *view);

G_END_DECLS

#endif /* __SUNONE_BACKEND_VIEW_H__ */
