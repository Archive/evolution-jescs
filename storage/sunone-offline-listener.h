/* sunone-offline-listener.c
 *
 * Copyright (C) 2005 Sun Microsystems, Inc
 *
 * AUTHORS
 *	Harry Lu <harry.lu@sun.com>
 */

#ifndef _SUNONE_OFFLINE_LISTNER_H_
#define _SUNONE_OFFLINE_LISTNER_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib-object.h>
#include <libedata-cal/e-data-cal-factory.h>

G_BEGIN_DECLS

#define SUNONE_OFFLINE_TYPE_LISTENER		        (sunone_offline_listener_get_type ())
#define SUNONE_OFFLINE_LISTENER(obj)		        ((G_TYPE_CHECK_INSTANCE_CAST((obj), SUNONE_OFFLINE_TYPE_LISTENER, SunOneOfflineListener)))
#define SUNONE_OFFLINE_LISTENER_CLASS(klass)	        (G_TYPE_CHECK_CLASS_CAST((klass), SUNONE_OFFLINE_TYPE_LISTENER, SunOneOfflineListenerClass))
#define SUNONE_OFFLINE_IS_LISTENER(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), SUNONE_OFFLINE_TYPE_LISTENER))
#define SUNONE_OFFLINE_IS_LISTENER_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((obj), SUNONE_OFFLINE_TYPE_LISTENER))

enum {
        OFFLINE_MODE=1,
        ONLINE_MODE
};


typedef struct _SunOneOfflineListener        SunOneOfflineListener;
typedef struct _SunOneOfflineListenerPrivate  SunOneOfflineListenerPrivate;
typedef struct _SunOneOfflineListenerClass   SunOneOfflineListenerClass;

struct _SunOneOfflineListener {
	GObject parent;
	SunOneOfflineListenerPrivate *priv;
  
};

struct _SunOneOfflineListenerClass {
	GObjectClass  parent_class;

};


GType sunone_offline_listener_get_type  (void);
SunOneOfflineListener  *sunone_offline_listener_new (EDataCalFactory *cal_factory);

void
sunone_is_offline (SunOneOfflineListener *offline_listener, int *state);

G_END_DECLS

#endif /* _SUNONE_OFFLINE_LISTNER_H_ */
