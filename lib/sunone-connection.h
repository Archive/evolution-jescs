/* sunone-connection.h
 *
 * Copyright (C) 2002-2004 Sun Microsystems, Inc
 *
 * AUTHORS
 *     Jack Jia <jack.jia@sun.com>
 *     Harry Lu <harry.lu@sun.com>
 *     Alfred Peng <alfred.peng@sun.com>
 *     Jedy Wang <jedy.wang@sun.com>
 *     Rodrigo Moya <rodrigo@ximian.com>
 *
 */

#ifndef SUNONE_CONNECTION_H
#define SUNONE_CONNECTION_H

#include <libical/ical.h>
#include <libecal/e-cal.h>

#include "sunone-ace.h"
#include "sunone-message.h"

G_BEGIN_DECLS

#define SUNONE_CONNECTION_TYPE            (sunone_connection_get_type ())
#define SUNONE_CONNECTION(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), SUNONE_CONNECTION_TYPE, SunOneConnection))
#define SUNONE_CONNECTION_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), SUNONE_CONNECTION_TYPE, SunOneConnectionClass))
#define IS_SUNONE_CONNECTION(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), SUNONE_CONNECTION_TYPE))
#define IS_SUNONE_CONNECTION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), SUNONE_CONNECTION_TYPE))

typedef struct _SunOneConnection      SunOneConnection;
typedef struct _SunOneConnectionClass SunOneConnectionClass;
typedef struct _SunOneConnectionPrivate      SunOneConnectionPrivate;

typedef struct {
	gchar *relative_calid;
	gchar *display_name;
	struct icaltimetype last_modified;
	struct icaltimetype created;
	gchar *language;
	gchar *primary_owner;
	GList *owners;
	gchar *timezone;
	GList *acls;
	gboolean is_resource;
} SunOneCalendarProperties;

typedef struct {
	gchar *calid;
	gchar *display_name;
} SunOneCalendarSubscription;

typedef struct {
	char *common_name;
	char *given_name;
	char *mail;
	char *preferred_language;
	char *surname;
	char *default_calendar;
	char *timezone;
	GList *subscriptions; /* list of SunOneCalendarSubscription's */
	char *freebusy;
	char *alarm_mail;
	gboolean single_calendar_tzid;
	gboolean allow_change_password;
	gboolean allow_create_calendars;
	gboolean allow_delete_calendars;
	gboolean allow_public_writable_calendars;
} SunOneConnectionPreferences;

struct _SunOneConnection {
	GObject object;

	/* private data */
	SunOneConnectionPrivate *priv;
};

struct _SunOneConnectionClass {
	GObjectClass parent_class;
};

typedef enum {
	PUBLISH = 1 << 0,
	REQUEST = 1 << 1,
	REPLY = 1 << 2,
	CANCEL = 1 << 3,
	MOVE = 1 << 4,
	COUNTER = 1 << 5,
} SunOneMethod;

typedef enum {
	TYPE_TODO = 1 << 0,
	TYPE_EVENT = 1 << 1,
	TYPE_ALL = 0x07
} SunOneCompType;

typedef enum {
	THISINSTANCE = 1,
	THISANDFUTURE = 2,
	THISANDPRIOR = 3,
	THISANDALL = 4
} SunOneModType;

typedef enum {
	REPLY_DECLINED = 1 << 0,
	REPLY_ACCEPTED = 1 << 1,
	REQUEST_COMPLETED = 1 << 2,
	REQUEST_NEEDS_ACTION = 1 << 3,
	REQUEST_NEEDS_NOACTION = 1 << 4,
	REQUEST_PENDING = 1 << 5,
	REQUEST_WAITFORREPLY = 1 << 6,
	REQUEST_ALL = 0x07
} SunOneCompState;

typedef enum {
	NAME_OR_ID,
	NAME,
	PRIMARY_OWNER,
	ID
} SunOneSearchField;

typedef enum {
	CONTAINS,
	BEGINS_WITH
}SunOneSearchBy;


GType           sunone_connection_get_type (void); 
SunOneConnection *sunone_connection_new (const char *server_uri, const char *proxy_uri,
							const char *username, const char *password);

guint sunone_connection_login (SunOneConnection *connection);
guint sunone_connection_logout (SunOneConnection *connection);
gboolean sunone_connection_is_open (SunOneConnection *connection);

SunOneConnectionPreferences *sunone_connection_get_preferences (SunOneConnection *connection);
guint             sunone_connection_set_preferences (SunOneConnection *connection,
						     SunOneConnectionPreferences *prefs);
void              sunone_connection_free_preferences (SunOneConnectionPreferences *prefs);
void              sunone_connection_free_subscription (SunOneCalendarSubscription *sub);


guint             sunone_connection_createcalendar (SunOneConnection *connection, const gchar *calid, gchar **real_calid);
guint             sunone_connection_deletecalendar (SunOneConnection *connection, const gchar *calid);

SunOneCalendarProperties *sunone_connection_get_calprops (SunOneConnection *connection,
							  const gchar *calid,
							  gboolean use_cache);
guint             sunone_connection_set_calprops (SunOneConnection *connection,
						  const gchar *calid,
						  SunOneCalendarProperties *calprops);
GList            *sunone_connection_list_calids (SunOneConnection *connection);
GList 			 *sunone_connection_search_calids (SunOneConnection *connection, SunOneSearchField field,
							SunOneSearchBy by, const char *search_str);
void              sunone_connection_free_calprops (SunOneCalendarProperties *calprops);

guint             sunone_connection_get_all_timezones (SunOneConnection *connection,
						       icalcomponent **icalcomp);

guint             sunone_connection_deleteevents_by_id (SunOneConnection *connection,
							const char *calid,
							const char *uid,
							const char *rid,
							SunOneModType mod);

guint             sunone_connection_deletetodos_by_id (SunOneConnection *connection,
						       const char *calid,
						       const char *uid,
						       const char *rid,
						       SunOneModType mod);

guint             sunone_connection_fetchcomponents_by_lastmod (SunOneConnection *connection,
								const char *calid,
								struct icaltimetype dtstart,
								struct icaltimetype dtend,
								SunOneCompType type,
								SunOneCompState state,
								icalcomponent **icalcomp,
								time_t *server_time);

guint
sunone_connection_fetch_deletedcomponents (SunOneConnection *connection,
				const char *calid,
				struct icaltimetype dtstart,
				struct icaltimetype dtend,
				SunOneCompType type,
				icalcomponent **icalcomp);


guint             sunone_connection_get_freebusy (SunOneConnection *connection,
						  const char *calid,
						  struct icaltimetype dtstart,
						  struct icaltimetype dtend,
						  icalcomponent **icalcomp);

guint             sunone_connection_import (SunOneConnection *connection,
					    const char *calid,
					    icalcomponent *icalcomp);
guint             sunone_connection_storeevents (SunOneConnection *connection,
						 const char *calid,
						 icalcomponent *icalcomp,
						 SunOneMethod method,
						 SunOneModType mod,
						 gboolean expand,
						 gboolean allday,
						 icalcomponent **return_icalcomp,
						 const char *account_email);
guint             sunone_connection_storetodos (SunOneConnection *connection, 
						const char *calid,
						icalcomponent *icalcomp,
						SunOneMethod method,
						SunOneModType mod,
						gboolean expand,
						gboolean allday,
						icalcomponent **return_icalcomp,
						const char *account_email);


guint             sunone_connection_verifyevents_by_ids (SunOneConnection *connection, 
							 const char *calid,
							 const char **uid,
							 const char **rid,
							 icalcomponent **icalcomp);
guint             sunone_connection_verifytodos_by_ids (SunOneConnection *connection,
							const char *calid,
							const char **uid,
							const char **rid,
							icalcomponent **icalcomp);
#if 0
guint             sunone_connection_fetchevents_by_id (SunOneConnection *connection,
				      const char *calid,
				      const char *uid,
//				      const char **ridv,
					  SunOneModType mod,
				      icalcomponent **icalcomp);
guint             sunone_connection_fetchtodos_by_id (SunOneConnection *connection,
				      const char *calid,
				      const char *uid,
//				      const char **ridv,
					  SunOneModType mod,
				      icalcomponent **icalcomp);
#endif
void              sunone_connection_add_wcap (SunOneConnection *connection, const char *key, gpointer wcap);
gpointer  sunone_connection_get_wcap (SunOneConnection *connection, const char *key);
guint		  sunone_connection_version (SunOneConnection *connection);
const char *sunone_connection_get_user (SunOneConnection *connection);
void		sunone_connection_set_user (SunOneConnection *connection, const char *user);
const char *sunone_connection_get_uid (SunOneConnection *connection);
const char *sunone_connection_get_wcap_version (SunOneConnection *connection);
void		sunone_connection_set_wcap_version (SunOneConnection *connection, const char *wcap_version);

guint  sunone_message_send (SunOneConnection *cnc, SoupMessage *msg);
guint  sunone_message_send_no_parse (SunOneConnection *cnc, SoupMessage *msg);

G_END_DECLS

#endif
