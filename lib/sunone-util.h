/* sunone-util.h
 *
 * Copyright (C) 2002-2004 Sun Microsystems, Inc
 *
 * AUTHORS
 *     Jack Jia <jack.jia@sun.com>
 *     Harry Lu <harry.lu@sun.com>
 *     Alfred Peng <alfred.peng@sun.com>
 *     Jedy Wang <jedy.wang@sun.com>
 *     Rodrigo Moya <rodrigo@ximian.com>
 *
 */

#ifndef SUNONE_UTIL_H
#define SUNONE_UTIL_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef ENABLE_IDN
#include <idn/api.h>
#endif

#include <libecal/e-cal-component.h>
#include "sunone-ace.h"
#include "sunone-connection.h"

G_BEGIN_DECLS

char *sunone_util_get_calid_from_uri (const char *uristr);
char *sunone_util_get_parameter_from_uri (const char *uristr, const char *name);
char *sunone_util_fix_calid (const char *calid);
void sunone_util_mangle_uid (ECalComponent *comp);
void sunone_util_unmangle_uid (ECalComponent *comp);

gboolean sunone_util_has_permissions (SunOneCalendarProperties *props, const char *user, SunOneACEContext context, SunOneACEPermission perms);
icalcomponent	*sunone_util_icalparser_parse_string (const char *str, int len);

#ifdef ENABLE_IDN
char * sunone_util_get_decoded_server_name (const char *server_port);
char * sunone_util_get_decoded_server_port_name (const char *server_port);
char * sunone_util_get_encoded_server_port_name (const char *server_port);
#endif

G_END_DECLS

#endif
