/* sunone-ace.h
 *
 * Copyright (C) 2002-2004 Sun Microsystems, Inc
 *
 * AUTHORS
 *     Jack Jia <jack.jia@sun.com>
 *     Harry Lu <harry.lu@sun.com>
 *     Alfred Peng <alfred.peng@sun.com>
 *     Jedy Wang <jedy.wang@sun.com>
 *     Rodrigo Moya <rodrigo@ximian.com>
 *
 */

#ifndef SUNONE_ACE_H
#define SUNONE_ACE_H

#include <glib-object.h>

G_BEGIN_DECLS

#define SUNONE_ACE_TYPE            (sunone_ace_get_type ())
#define SUNONE_ACE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), SUNONE_ACE_TYPE, SunOneACE))
#define SUNONE_ACE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), SUNONE_ACE_TYPE, SunOneACEClass))
#define IS_SUNONE_ACE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), SUNONE_ACE_TYPE))
#define IS_SUNONE_ACE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), SUNONE_ACE_TYPE))

typedef struct _SunOneACE      SunOneACE;
typedef struct _SunOneACEClass SunOneACEClass;
typedef struct _SunOneACEPrivate      SunOneACEPrivate;

typedef enum {
	SUNONE_ACE_ACCESSTYPE_INVALID = -1,
	SUNONE_ACE_ACCESSTYPE_GRANT,
	SUNONE_ACE_ACCESSTYPE_DENY
} SunOneACEAccessType;

typedef enum {
	SUNONE_ACE_USERTYPE_INVALID = -1,
	SUNONE_ACE_USERTYPE_USER,
	SUNONE_ACE_USERTYPE_DOMAIN,
	SUNONE_ACE_USERTYPE_ALL_USERS,
	SUNONE_ACE_USERTYPE_PRIMARY_OWNERS,
	SUNONE_ACE_USERTYPE_OWNERS,
	SUNONE_ACE_USERTYPE_NONOWNERS
} SunOneACEUserType;

typedef enum {
	SUNONE_ACE_CONTEXT_INVALID = -1,
	SUNONE_ACE_CONTEXT_ENTIRE_CALENDAR,
	SUNONE_ACE_CONTEXT_CALENDAR_COMPONENTS,
	SUNONE_ACE_CONTEXT_CALENDAR_PROPERTIES
} SunOneACEContext;

typedef enum {
	SUNONE_ACE_PERMISSION_NONE = 0,
	SUNONE_ACE_PERMISSION_CANCEL = 1 << 0,
	SUNONE_ACE_PERMISSION_DELETE = 1 << 1,
	SUNONE_ACE_PERMISSION_REPLY = 1 << 2,
	SUNONE_ACE_PERMISSION_FREEBUSY = 1 << 3,
	SUNONE_ACE_PERMISSION_INVITE = 1 << 4,
	SUNONE_ACE_PERMISSION_READ = 1 << 5,
	SUNONE_ACE_PERMISSION_SCHEDULE = 1 << 6,
	SUNONE_ACE_PERMISSION_WRITE = 1 << 7,
	SUNONE_ACE_PERMISSION_ALL = SUNONE_ACE_PERMISSION_CANCEL | SUNONE_ACE_PERMISSION_DELETE |
	                            SUNONE_ACE_PERMISSION_REPLY | SUNONE_ACE_PERMISSION_FREEBUSY |
	                            SUNONE_ACE_PERMISSION_INVITE | SUNONE_ACE_PERMISSION_READ |
	                            SUNONE_ACE_PERMISSION_SCHEDULE | SUNONE_ACE_PERMISSION_WRITE,
	SUNONE_ACE_PERMISSION_READWRITE = SUNONE_ACE_PERMISSION_READ | SUNONE_ACE_PERMISSION_WRITE
} SunOneACEPermission;

struct _SunOneACE {
	GObject object;

	/* private data */
	SunOneACEPrivate *priv;
};

struct _SunOneACEClass {
	GObjectClass parent_class;
};

GType             sunone_ace_get_type (void); 

SunOneACE          *sunone_ace_new (void);
SunOneACE          *sunone_ace_new_from_string (const gchar *str);
SunOneACE          *sunone_ace_clone (SunOneACE *ace);
gboolean            sunone_ace_set_from_string (SunOneACE *ace, const gchar *str);
gchar              *sunone_ace_to_string (SunOneACE *ace);
SunOneACEAccessType sunone_ace_get_access_type (SunOneACE *ace);
void                sunone_ace_set_access_type (SunOneACE *ace, SunOneACEAccessType type);
SunOneACEUserType   sunone_ace_get_user_type (SunOneACE *ace);
void                sunone_ace_set_user_type (SunOneACE *ace, SunOneACEUserType type);
const gchar        *sunone_ace_get_user_name (SunOneACE *ace);
void                sunone_ace_set_user_name (SunOneACE *ace, const gchar *name);
SunOneACEContext    sunone_ace_get_context (SunOneACE *ace);
void                sunone_ace_set_context (SunOneACE *ace, SunOneACEContext context);
SunOneACEPermission sunone_ace_get_permissions (SunOneACE *ace);
void                sunone_ace_set_permissions (SunOneACE *ace, SunOneACEPermission perms);

G_END_DECLS

#endif
