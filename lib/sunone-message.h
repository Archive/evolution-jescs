/* sunone-message.h
 *
 * Copyright (C) 2002-2004 Sun Microsystems, Inc
 *
 * AUTHORS
 *     Jack Jia <jack.jia@sun.com>
 *     Harry Lu <harry.lu@sun.com>
 *     Alfred Peng <alfred.peng@sun.com>
 *     Rodrigo Moya <rodrigo@ximian.com>
 *
 */

#ifndef SUNONE_MESSAGE_H
#define SUNONE_MESSAGE_H

#include <libsoup/soup-message.h>
#include <libsoup/soup-uri.h>
#include "sunone-error-codes.h"

G_BEGIN_DECLS

SoupMessage    *sunone_message_new_from_uri (SoupURI *suri, const char *method);
SoupMessage    *sunone_message_new_full_from_uri (SoupURI *suri, const char *method, const char *content_type, SoupMemoryUse req_use, char *body, gulong length);
void			parse_server_response (SoupMessage *msg, guint *status);
void			setup_message (SoupMessage *msg);

G_END_DECLS

#endif
