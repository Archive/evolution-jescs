# Process this file with autoconf to produce a configure script.
AC_PREREQ(2.52)

AC_INIT(evolution-jescs, 2.28.0, http://bugzilla.gnome.org/enter_bug.cgi?product=evolution-jescs)
AC_CONFIG_SRCDIR(README)
AM_INIT_AUTOMAKE(AC_PACKAGE_NAME, AC_PACKAGE_VERSION)
AM_INIT_AUTOMAKE([foreign])

AM_CONFIG_HEADER(config.h)

EVOLUTION_BASE_VERSION=2.28
AC_DEFINE_UNQUOTED(EVOLUTION_BASE_VERSION,"$EVOLUTION_BASE_VERSION",Evolution base version)

DATASERVER_API_VERSION=1.2
AC_DEFINE_UNQUOTED(DATASERVER_API_VERSION,"$DATASERVER_API_VERSION",Evolution Dataserver API version)
AC_SUBST(DATASERVER_API_VERSION)

DATASERVER_VERSION=2.28
AC_SUBST(DATASERVER_VERSION)

SOUP_VERSION=2.4

AC_ARG_WITH(sunone-debug,     [  --with-sunone-debug              Allow debugging])
case $withval in
no)
	AC_DEFINE(G_DISABLE_ASSERT)
	AC_DEFINE(G_DISABLE_CHECKS)
	;;
*)
	AC_DEFINE(SUNONE_DEBUG, 1, [Define if you want SUNONE_DEBUG to be available])
	;;
esac


AC_MSG_CHECKING(Evolution version)
EVOLUTION_VERSION=`pkg-config --modversion evolution-shell 2>/dev/null`
if test -z "$EVOLUTION_VERSION"; then
	AC_MSG_ERROR(Evolution development libraries not installed)
fi
AC_SUBST(EVOLUTION_VERSION)
AC_MSG_RESULT($EVOLUTION_VERSION)

BASE_VERSION=$EVOLUTION_BASE_VERSION
AC_DEFINE_UNQUOTED(BASE_VERSION, "$BASE_VERSION", evolution-jescs base version)
AC_SUBST(BASE_VERSION)

dnl Initialize maintainer mode
AM_MAINTAINER_MODE

AC_ISC_POSIX
AC_PROG_CC
AC_STDC_HEADERS
AC_PROG_INSTALL
AC_PROG_LN_S
AC_PROG_MAKE_SET

dnl Initialize libtool
AM_DISABLE_STATIC
AM_PROG_LIBTOOL

dnl ****
dnl i18n
dnl ****
AC_PROG_INTLTOOL([0.35.0])
AM_GLIB_GNU_GETTEXT

GETTEXT_PACKAGE=evolution-jescs-$BASE_VERSION
AC_SUBST(GETTEXT_PACKAGE)
AC_DEFINE_UNQUOTED(GETTEXT_PACKAGE, "$GETTEXT_PACKAGE", [Package name for gettext])

localedir='$(prefix)/$(DATADIRNAME)/locale'
AC_SUBST(localedir)

dnl *************************
dnl CFLAGS and LIBS and stuff
dnl *************************

GNOME_COMPILE_WARNINGS(maximum)
CFLAGS="$CFLAGS $WARN_CFLAGS"
case $CFLAGS in
*-Wall*)
	CFLAGS="$CFLAGS -Wno-sign-compare"
	;;
esac
AC_DEFINE(G_DISABLE_DEPRECATED,1,[No deprecated glib functions])
AC_DEFINE(GDK_DISABLE_DEPRECATED,1,[No deprecated gdk functions])
AC_DEFINE(GTK_DISABLE_DEPRECATED,1,[No deprecated gtk functions])
AC_DEFINE(GNOME_DISABLE_DEPRECATED,1,[No deprecated gnome functions])
AC_DEFINE(BONOBO_DISABLE_DEPRECATED,1,[No deprecated bonobo functions])
AC_DEFINE(BONOBO_UI_DISABLE_DEPRECATED,1,[No deprecated bonoboui functions])
AC_DEFINE(GCONF_DISABLE_DEPRECATED,1,[No deprecated gconf functions])
AC_DEFINE(LIBGLADE_DISABLE_DEPRECATED,1,[No deprecated libglade functions])

AC_DEFINE(HANDLE_LIBICAL_MEMORY, 1, [Define it once memory returned by libical is free'ed properly])

AM_PATH_GLIB_2_0
AM_PATH_ORBIT2
AC_PATH_PROG(GCONFTOOL, gconftool-2, no)
AM_GCONF_SOURCE_2


dnl **************************************************
dnl IDN support.
dnl **************************************************

msg_idn="no"

AC_ARG_WITH(idn, 
	[  --enable-idn=[yes/no]   	enable IDN support using idnkit ],
	,, enable_idn=no)


if test "x$enable_idn" = "xyes"; then
	PKG_CHECK_MODULES(IDN, idnkit, msg_idn="yes")
        AC_DEFINE(ENABLE_IDN, 1, [Enable IDN support])
fi

CONNECTOR_DATADIR='$(datadir)/evolution-jescs/$(BASE_VERSION)'
AC_SUBST(CONNECTOR_DATADIR)

idldir="`pkg-config --variable=idldir evolution-shell`"
AC_SUBST(idldir)

IDL_INCLUDES="`pkg-config --variable=IDL_INCLUDES evolution-shell`"
AC_SUBST(IDL_INCLUDES)

privlibdir="`pkg-config --variable=privlibdir evolution-shell`"
AC_SUBST(privlibdir)

privlibexecdir="`pkg-config --variable=privlibexecdir evolution-shell`"
AC_SUBST(privlibexecdir)

privincludedir="`pkg-config --variable=privincludedir evolution-shell`"
AC_SUBST(privincludedir)

imagesdir="`pkg-config --variable=imagesdir evolution-shell`"
AC_SUBST(imagesdir)

serverdir="$libdir/bonobo/servers"
AC_SUBST(serverdir)

camel_providerdir="`pkg-config --variable=camel_providerdir camel-provider-$DATASERVER_API_VERSION`"
AC_SUBST(camel_providerdir)

PKG_CHECK_MODULES(CALENDAR, libedata-cal-$DATASERVER_API_VERSION evolution-shell libsoup-$SOUP_VERSION)
AC_SUBST(CALENDAR_CFLAGS)
AC_SUBST(CALENDAR_LIBS)

PKG_CHECK_MODULES(CAMEL, camel-provider-$DATASERVER_API_VERSION glib-2.0)
AC_SUBST(CAMEL_CFLAGS)
AC_SUBST(CAMEL_LIBS)

IDNKIT=""
if test "x$msg_idn" = "xyes"; then
	IDNKIT="idnkit"
fi

PKG_CHECK_MODULES(STORAGE_EVO, evolution-shell libedata-cal-$DATASERVER_API_VERSION libedataserverui-$DATASERVER_API_VERSION libsoup-$SOUP_VERSION camel-provider-$DATASERVER_API_VERSION libglade-2.0 libgnomeui-2.0 $IDNKIT)

dnl Hack for Solaris build
dnl since we don't ship .la files for Solaris
STORAGE_EXTRA_LIBS="-leutil -lemiscwidgets"
STORAGE_EVO_LIBS="$STORAGE_EVO_LIBS $STORAGE_EXTRA_LIBS"
dnl Hack finished

AC_SUBST(STORAGE_EVO_CFLAGS)
AC_SUBST(STORAGE_EVO_LIBS)

PKG_CHECK_MODULES(LIBJESCS, libsoup-$SOUP_VERSION libedata-cal-$DATASERVER_API_VERSION libgnomeui-2.0 $IDNKIT)
AC_SUBST(LIBJESCS_CFLAGS)
AC_SUBST(LIBJESCS_LIBS)

AC_MSG_CHECKING(for libgnomeui server directory)
GNOMEUI_SERVERDIR="`$PKG_CONFIG --variable=libgnomeui_serverdir libgnomeui-2.0`"
AC_MSG_RESULT($GNOMEUI_SERVERDIR)
AC_DEFINE_UNQUOTED(GNOMEUI_SERVERDIR, "$GNOMEUI_SERVERDIR", [Path where we can find gnome_segv2])

dnl *******
dnl gtk-doc
dnl *******
dnl Putting a space before the macro call here makes gnome-autogen not
dnl notice it, which makes it not run gtkdocize and copy gtk-doc.make
dnl into the top level where I don't want it.
 GTK_DOC_CHECK([1.0])


dnl ******************************
dnl Makefiles
dnl ******************************

AC_OUTPUT([
Makefile
lib/Makefile
camel/Makefile
calendar/Makefile
data/Makefile
storage/Makefile
po/Makefile.in
])
