/* cal-backend-wcap-tasks.h
 *
 * Copyright (C) 2002-2004 Sun Microsystems, Inc
 *
 * AUTHORS
 *     Jack Jia <jack.jia@sun.com>
 *     Harry Lu <harry.lu@sun.com>
 *     Alfred Peng <alfred.peng@sun.com>
 *     Jedy Wang <jedy.wang@sun.com>
 *     Rodrigo Moya <rodrigo@ximian.com>
 *     JP Rosevear <jpr@ximian.com>
 *
 */

#ifndef CAL_BACKEND_WCAP_TASKS_H
#define CAL_BACKEND_WCAP_TASKS_H

#include "cal-backend-wcap.h"

G_BEGIN_DECLS

#define CAL_BACKEND_WCAP_TASKS_TYPE            (cal_backend_wcap_tasks_get_type ())
#define CAL_BACKEND_WCAP_TASKS(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), CAL_BACKEND_WCAP_TASKS_TYPE,	CalBackendWCAPTasks))
#define CAL_BACKEND_WCAP_TASKS_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), CAL_BACKEND_WCAP_TASKS_TYPE,	CalBackendWCAPClass))
#define IS_CAL_BACKEND_WCAP_TASKS(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CAL_BACKEND_WCAP_TASKS_TYPE))
#define IS_CAL_BACKEND_WCAP_TASKS_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), CAL_BACKEND_WCAP_TASKS_TYPE))

typedef struct _CalBackendWCAPTasks        CalBackendWCAPTasks;
typedef struct _CalBackendWCAPTasksClass   CalBackendWCAPTasksClass;

struct _CalBackendWCAPTasks {
	CalBackendWCAP parent;

	/* private data */
};

struct _CalBackendWCAPTasksClass {
	CalBackendWCAPClass parent_class;
};

GType cal_backend_wcap_tasks_get_type (void); 

G_END_DECLS

#endif
