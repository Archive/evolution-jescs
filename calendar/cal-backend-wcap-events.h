/* cal-backend-wcap-events.h
 *
 * Copyright (C) 2002-2004 Sun Microsystems, Inc
 *
 * AUTHORS
 *     Jack Jia <jack.jia@sun.com>
 *     Harry Lu <harry.lu@sun.com>
 *     Alfred Peng <alfred.peng@sun.com>
 *     Jedy Wang <jedy.wang@sun.com>
 *     Rodrigo Moya <rodrigo@ximian.com>
 *     JP Rosevear <jpr@ximian.com>
 *
 */

#ifndef CAL_BACKEND_WCAP_EVENTS_H
#define CAL_BACKEND_WCAP_EVENTS_H

#include "cal-backend-wcap.h"

G_BEGIN_DECLS

#define CAL_BACKEND_WCAP_EVENTS_TYPE            (cal_backend_wcap_events_get_type ())
#define CAL_BACKEND_WCAP_EVENTS(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), CAL_BACKEND_WCAP_EVENTS_TYPE,	CalBackendWCAPEvents))
#define CAL_BACKEND_WCAP_EVENTS_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), CAL_BACKEND_WCAP_EVENTS_TYPE,	CalBackendWCAPClass))
#define IS_CAL_BACKEND_WCAP_EVENTS(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CAL_BACKEND_WCAP_EVENTS_TYPE))
#define IS_CAL_BACKEND_WCAP_EVENTS_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), CAL_BACKEND_WCAP_EVENTS_TYPE))

typedef struct _CalBackendWCAPEvents        CalBackendWCAPEvents;
typedef struct _CalBackendWCAPEventsClass   CalBackendWCAPEventsClass;

struct _CalBackendWCAPEvents {
	CalBackendWCAP parent;

	/* private data */
};

struct _CalBackendWCAPEventsClass {
	CalBackendWCAPClass parent_class;
};

GType cal_backend_wcap_events_get_type (void); 
ECalBackendSyncStatus cal_backend_wcap_events_update_objects (ECalBackendSync *backend, EDataCal *cal,
								const char *calobj, CalObjModType mod, char **old_object, char **new_object);

G_END_DECLS

#endif
