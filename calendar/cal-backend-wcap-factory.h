/* cal-backend-wcap-factory.h
 *
 * Copyright (C) 2002-2004 Sun Microsystems, Inc
 *
 *
 * AUTHORS
 *     Jack Jia <jack.jia@sun.com>
 *     Harry Lu <harry.lu@sun.com>
 *     Alfred Peng <alfred.peng@sun.com>
 *     Jedy Wang <jedy.wang@sun.com>
 */

#ifndef CAL_BACKEND_WCAP_FACTORY_H
#define CAL_BACKEND_WCAP_FACTORY_H

#include <glib-object.h>
#include <libedata-cal/e-cal-backend-factory.h>

G_BEGIN_DECLS

#define TYPE_CAL_BACKEND_WCAP_FACTORY        (cal_backend_wcap_factory_get_type ())
#define CAL_BACKEND_WCAP_FACTORY(o)          (G_TYPE_CHECK_INSTANCE_CAST ((o), TYPE_CAL_BACKEND_WCAP_FACTORY, CalBackendWcapFactory))
#define CAL_BACKEND_WCAP_FACTORY_CLASS(k)    (G_TYPE_CHECK_CLASS_CAST((k), TYPE_CAL_BACKEND_WCAP_FACTORY, CalBackendWcapFactoryClass))
#define IS_CAL_BACKEND_WCAP_FACTORY(o)       (G_TYPE_CHECK_INSTANCE_TYPE ((o), TYPE_CAL_BACKEND_WCAP_FACTORY))
#define IS_CAL_BACKEND_WCAP_FACTORY_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE ((k), TYPE_CAL_BACKEND_WCAP_FACTORY))
#define CAL_BACKEND_WCAP_FACTORY_GET_CLASS(k) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_CAL_BACKEND_WCAP_FACTORY, CalBackendWcapFactoryClass))

typedef struct {
	ECalBackendFactory            parent_object;
} CalBackendWcapFactory;

typedef struct {
	ECalBackendFactoryClass parent_class;
} CalBackendWcapFactoryClass;

GType	events_backend_wcap_factory_get_type (void);
GType	tasks_backend_wcap_factory_get_type (void);

G_END_DECLS

#endif /* CAL_BACKEND_WCAP_FACTORY_H */
