/* cal-backend-wcap.h
 *
 * Copyright (C) 2002-2004 Sun Microsystems, Inc
 *
 * AUTHORS
 *     Jack Jia <jack.jia@sun.com>
 *     Harry Lu <harry.lu@sun.com>
 *     Alfred Peng <alfred.peng@sun.com>
 *     Jedy Wang <jedy.wang@sun.com>
 *     Rodrigo Moya <rodrigo@ximian.com>
 *     JP Rosevear <jpr@ximian.com>
 *
 */

#ifndef CAL_BACKEND_WCAP_H
#define CAL_BACKEND_WCAP_H

#include <libedata-cal/e-cal-backend-sync.h>
#include <libedata-cal/e-cal-backend.h>
#include "lib/sunone-connection.h"

G_BEGIN_DECLS

#define CAL_BACKEND_WCAP_TYPE            (cal_backend_wcap_get_type ())
#define CAL_BACKEND_WCAP(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), CAL_BACKEND_WCAP_TYPE,	CalBackendWCAP))
#define CAL_BACKEND_WCAP_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), CAL_BACKEND_WCAP_TYPE,	CalBackendWCAPClass))
#define IS_CAL_BACKEND_WCAP(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CAL_BACKEND_WCAP_TYPE))
#define IS_CAL_BACKEND_WCAP_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), CAL_BACKEND_WCAP_TYPE))

typedef struct _CalBackendWCAP        CalBackendWCAP;
typedef struct _CalBackendWCAPClass   CalBackendWCAPClass;
typedef struct _CalBackendWCAPPrivate        CalBackendWCAPPrivate;

struct _CalBackendWCAP {
	ECalBackendSync parent;

	/* private data */
	CalBackendWCAPPrivate *priv;
};

struct _CalBackendWCAPClass {
	ECalBackendSyncClass parent_class;
};

void				cal_backend_wcap_write_cache (CalBackendWCAP *wcap);
void				cal_backend_wcap_add_component (CalBackendWCAP *wcap, icalcomponent *icalcomp,
													gboolean process, char **old_object, char **new_object);
void				cal_backend_wcap_remove_component (CalBackendWCAP *wcap, const char *uid, CalObjModType mod, char **old_object);
void				cal_backend_wcap_verify_exists_uid (CalBackendWCAP *wcap, const char *uid);
gint				cal_backend_wcap_instance_list_compare (gconstpointer a, gconstpointer b);
gboolean			cal_backend_wcap_poll_cb (gpointer data);
gboolean			cal_backend_wcap_is_online (CalBackendWCAP *wcap);
char				*cal_backend_wcap_get_rid_string (ECalComponent *comp);
const char			*cal_backend_wcap_get_uri (CalBackendWCAP *wcap);
const char			*cal_backend_wcap_get_calid (CalBackendWCAP *wcap);
const char			*cal_backend_wcap_get_alarm_email (CalBackendWCAP *wcap);
const char			*cal_backend_wcap_get_account_email (CalBackendWCAP *wcap);
const char			*cal_backend_wcap_get_cache_name (CalBackendWCAP *wcap);
CalMode				cal_backend_wcap_get_mode (ECalBackend *backend);
GType				cal_backend_wcap_get_type (void); 
GHashTable			*cal_backend_wcap_get_objects (CalBackendWCAP *wcap);
GHashTable			*cal_backend_wcap_get_instances (CalBackendWCAP *wcap);
icaltimezone		*cal_backend_wcap_get_default_zone (CalBackendWCAP *wcap);
icaltimezone		*cal_backend_wcap_get_server_default_zone (CalBackendWCAP *wcap);
icaltimezone		*cal_backend_wcap_get_timezone_from_tzid (CalBackendWCAP *wcap, const char *tzid,
															gboolean local_only);
SunOneCompType		cal_backend_wcap_get_comp_type (CalBackendWCAP *wcap);
SunOneModType		cal_backend_wcap_to_sunone_mod (CalObjModType mod, ECalComponent *comp);
SunOneMethod		cal_backend_wcap_guess_method (CalBackendWCAP *wcap, EDataCal *cal, ECalComponent *comp);
SunOneConnection			*cal_backend_wcap_get_connection (CalBackendWCAP *wcap);
ECalBackendSyncStatus		cal_backend_wcap_result_from_error (guint error_code);
ECalBackendSyncStatus		cal_backend_wcap_get_open_error (CalBackendWCAP *wcap);
SunOneCalendarProperties	*cal_backend_wcap_get_props (CalBackendWCAP *wcap);
gboolean					cal_backend_wcap_need_import (CalBackendWCAP *wcap, ECalComponent *comp);
ECalBackendSyncStatus		cal_backend_wcap_handle_import (CalBackendWCAP *wcap, ECalComponent *comp, CalObjModType mod, 
														icalcomponent *toplevel_comp, icalcomponent *subcomp,
														icalcomponent_kind child_kind);

#define IS_WCAP_2_0(wcap) ( !strncmp (sunone_connection_get_wcap_version (cal_backend_wcap_get_connection (wcap)), "2.0", 3) )

G_END_DECLS

#endif
