/* cal-backend-wcap.c
 *
 * Copyright (C) 2002-2004 Sun Microsystems, Inc
 *
 * AUTHORS
 *     Jack Jia <jack.jia@sun.com>
 *     Harry Lu <harry.lu@sun.com>
 *     Alfred Peng <alfred.peng@sun.com>
 *     Jedy Wang <jedy.wang@sun.com>
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>

#include "cal-backend-wcap.h"
#include "cal-backend-wcap-events.h"
#include "cal-backend-wcap-tasks.h"
#include "cal-backend-wcap-factory.h"

static void
cal_backend_wcap_factory_instance_init (CalBackendWcapFactory *factory)
{
}

static const char *
_get_protocol (ECalBackendFactory *factory)
{
	return "wcap";
}

static ECalBackend*
_tasks_new_backend (ECalBackendFactory *factory, ESource *source)
{
	return g_object_new (CAL_BACKEND_WCAP_TASKS_TYPE, 
			     "source", source, 
			     "kind", ICAL_VTODO_COMPONENT, NULL);
}

static icalcomponent_kind 
_tasks_get_kind (ECalBackendFactory *factory) 
{ 
	return ICAL_VTODO_COMPONENT; 
}

static ECalBackend*
_events_new_backend (ECalBackendFactory *factory, ESource *source)
{
	return g_object_new (CAL_BACKEND_WCAP_EVENTS_TYPE, 
			     "source", source, 
			     "kind", ICAL_VEVENT_COMPONENT, NULL);
}

static icalcomponent_kind 
_events_get_kind (ECalBackendFactory *factory) 
{ 
	return ICAL_VEVENT_COMPONENT; 
}

static void
tasks_backend_wcap_factory_class_init (CalBackendWcapFactoryClass *klass)
{
	E_CAL_BACKEND_FACTORY_CLASS (klass)->get_protocol = _get_protocol;
	E_CAL_BACKEND_FACTORY_CLASS (klass)->get_kind     = _tasks_get_kind;
	E_CAL_BACKEND_FACTORY_CLASS (klass)->new_backend = _tasks_new_backend;
}

static void
events_backend_wcap_factory_class_init (CalBackendWcapFactoryClass *klass)
{
	E_CAL_BACKEND_FACTORY_CLASS (klass)->get_protocol = _get_protocol;
	E_CAL_BACKEND_FACTORY_CLASS (klass)->get_kind     = _events_get_kind;
	E_CAL_BACKEND_FACTORY_CLASS (klass)->new_backend = _events_new_backend;
}

GType
events_backend_wcap_factory_get_type (void)
{
	static GType type = 0;

	if (!type) {
		GTypeInfo info = {
			sizeof (CalBackendWcapFactoryClass),
			NULL, /* base_class_init */
			NULL, /* base_class_finalize */
			(GClassInitFunc)  events_backend_wcap_factory_class_init,
			NULL, /* class_finalize */
			NULL, /* class_data */
			sizeof (ECalBackend),
			0,    /* n_preallocs */
			(GInstanceInitFunc) cal_backend_wcap_factory_instance_init
		};

		type = g_type_register_static (E_TYPE_CAL_BACKEND_FACTORY,
					       "ECalBackendWcapEventsFactory",
					       &info, 0);
	}
	return type;
}

GType
tasks_backend_wcap_factory_get_type (void)
{
	static GType type = 0;

	if (!type) {
		GTypeInfo info = {
			sizeof (CalBackendWcapFactoryClass),
			NULL, /* base_class_init */
			NULL, /* base_class_finalize */
			(GClassInitFunc)  tasks_backend_wcap_factory_class_init,
			NULL, /* class_finalize */
			NULL, /* class_data */
			sizeof (ECalBackend),
			0,    /* n_preallocs */
			(GInstanceInitFunc) cal_backend_wcap_factory_instance_init
		};

		type = g_type_register_static (E_TYPE_CAL_BACKEND_FACTORY,
					       "ECalBackendWcapTasksFactory",
					       &info, 0);
	}
	return type;
}
